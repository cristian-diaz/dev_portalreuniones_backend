﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using System.Threading.Tasks;

namespace API.PortalReuniones.BussinesRules
{
    public class ReunionArchivosBussinesRules
    {
        private readonly IReunionArchivosRepositories _repository;
        public ReunionArchivosBussinesRules(IReunionArchivosRepositories repository)
        {
            _repository = repository;
        }

        public async Task<PagedResult<ReunionArchivos>> ConsultarReunionArchivos(ReunionArchivosFiltro data)
        {
            return await _repository.ConsultarReunionArchivos(data);
        }
    }
}

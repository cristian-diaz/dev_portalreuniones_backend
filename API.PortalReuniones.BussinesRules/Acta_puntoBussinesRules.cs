﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace API.PortalReuniones.BussinesRules
{
    public class Acta_puntoBussinesRules : IActa_puntoRepositories
    {
        readonly PortalReunionesContext db;

        public Acta_puntoBussinesRules(PortalReunionesContext context)
        {
            db = context;
        }

        public async Task<ActaPunto> GuardarActa_punto(ActaPunto viewModel)
        {
            await db.Acta_punto.AddAsync(viewModel);
            await db.SaveChangesAsync();
            return viewModel;
        }

        public async Task<ActaPunto> ActualizarActa_punto(ActaPunto viewModel)
        {
            var dato = db.Acta_punto.Where(m => m.IdActa == viewModel.IdActa && m.IdPuntosReunion == viewModel.IdPuntosReunion).FirstOrDefault();
            if (viewModel.IdActa > 0)
                dato.IdActa = viewModel.IdActa;
            if (viewModel.IdPuntosReunion > 0)
                dato.IdPuntosReunion = viewModel.IdPuntosReunion;

            db.Entry(dato).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return dato;
        }
    }
}

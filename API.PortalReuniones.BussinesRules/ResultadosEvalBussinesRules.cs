﻿using API.PortalReuniones.Entities.EntitiesExtension;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using System.Threading.Tasks;

namespace API.PortalReuniones.BussinesRules
{
    public class ResultadosEvalBussinesRules
    {
        private readonly IResultadosEvalRepositories _repository;

        public ResultadosEvalBussinesRules(IResultadosEvalRepositories repository)
        {
            _repository = repository;
        }

        public async Task<PagedResult<ReporteEvaluaciones>> ConsultarReporteEvaluacion(ReporteEvaluacionFiltro data)
        {
            if (data.Page == 0)
            {
                data.Page = 1;
            }

            if (data.Size == 0)
            {
                data.Size = 1000;
            }
            return await _repository.ConsultarReporteEvaluacion(data);
        }
    }
}

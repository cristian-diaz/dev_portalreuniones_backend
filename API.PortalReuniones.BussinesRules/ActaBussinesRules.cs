﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using System.Threading.Tasks;

namespace API.PortalReuniones.BussinesRules
{
    public class ActaBussinesRules
    {
        private readonly IActaRepositories _repository;

        public ActaBussinesRules(IActaRepositories repository)
        {
            _repository = repository;
        }

        public async Task<int> GuardarActa(Acta data)
        {
            return await _repository.GuardarActa(data);
        }

        public async Task<PagedResult<Acta>> ConsultarActa(ActaFiltro data)
        {
            return await _repository.ConsultarActa(data);
        }

        public async Task<int> ActualizarActa(Acta data)
        {
            return await _repository.ActualizarActa(data);
        }
    }
}

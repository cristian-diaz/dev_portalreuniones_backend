﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using System.Threading.Tasks;

namespace API.PortalReuniones.BussinesRules
{
    public class ItemEvaluacionBussinesRules
    {
        private readonly IItemEvaluacionRepositories _repository;

        public ItemEvaluacionBussinesRules(IItemEvaluacionRepositories repository)
        {
            _repository = repository;
        }

        public async Task<PagedResult<ItemEvaluacion>> ConsultarItemEvaluacion(ItemEvaluacionFiltro data)
        {
            return await _repository.ConsultarItemEvaluacion(data);
        }
    }
}

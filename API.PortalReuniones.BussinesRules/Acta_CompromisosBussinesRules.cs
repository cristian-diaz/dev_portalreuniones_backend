﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace API.PortalReuniones.BussinesRules
{
    public class Acta_CompromisosBussinesRules
    {
        private readonly IActa_CompromisosRepositories _repository;

        public Acta_CompromisosBussinesRules(IActa_CompromisosRepositories repository)
        {
            _repository = repository;
        }

        public async Task<int> GuardarActa_Compromisos(ActaCompromisos data)
        {
            return await _repository.GuardarActa_Compromisos(data);
        }

        public async Task<int> ActualizarActa_Compromisos(ActaCompromisos data)
        {
            return await _repository.ActualizarActa_Compromisos(data);
        }
    }
}

﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace API.PortalReuniones.BussinesRules
{
    public class Acta_ParticipanteBussinesRules : IActa_ParticipanteRepositories
    {
        readonly PortalReunionesContext db;

        public Acta_ParticipanteBussinesRules(PortalReunionesContext context)
        {
            db = context;
        }

        public async Task<ActaParticipante> GuardarActa_Participante(ActaParticipante viewModel)
        {
            await db.Acta_Participante.AddAsync(viewModel);
            await db.SaveChangesAsync();
            return viewModel;
        }

        public async Task<ActaParticipante> ActualizarActa_Participante(ActaParticipante viewModel)
        {
            var dato = db.Acta_Participante.Where(m => m.IdActa == viewModel.IdActa && m.Login == viewModel.Login).FirstOrDefault();
            if (viewModel.IdActa > 0)
                dato.IdActa = viewModel.IdActa;
            if (viewModel.Login != null)
                dato.Login = viewModel.Login;

            db.Entry(dato).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return dato;
        }
    }
}

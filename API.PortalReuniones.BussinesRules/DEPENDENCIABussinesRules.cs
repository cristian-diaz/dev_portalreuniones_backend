﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.PortalReuniones.BussinesRules
{
    public class DependenciaBussinesRules
    {
        private readonly IDependenciaRepositories _repository;

        public DependenciaBussinesRules(IDependenciaRepositories repositories)
        {
            _repository = repositories;
        }

        public async Task<PagedResult<Dependencia>> GetDepencias()
        {
            return await _repository.GetDepencias();
        }

        public async Task<PagedResult<Dependencia>> ConsultarDEPENDENCIA(DEPENDENCIAFiltro data)
        {
            return await _repository.ConsultarDEPENDENCIA(data);
        }
    }
}

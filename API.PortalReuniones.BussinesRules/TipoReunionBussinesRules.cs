﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;

namespace API.PortalReuniones.BussinesRules
{
    public class TipoReunionBussinesRules
    {
        private readonly ITipoReunionRepositories _repository;

        public TipoReunionBussinesRules(ITipoReunionRepositories repository)
        {
            _repository = repository;
        }

        public async Task<PagedResult<TipoReunion>> ConsultarTipoReunion(TipoReunionFiltro data)
        {
            return await _repository.ConsultarTipoReunion(data);
        }

        public int PaginadoTotal(List<TipoReunion> tipoReuniones, int paginado)
        {
            int cantidad = tipoReuniones.Count();
            return Convert.ToInt32(Math.Ceiling((decimal)cantidad / (decimal)paginado));
        }

        public async Task<PagedResult<TipoReunion>> ConsultarTipoReunionFiltro(PresentarTipoReunionFiltro data)
        {
            return await _repository.ConsultarTipoReunionFiltro(data);
        }

        public async Task<int> GuardarTipoReunion(TipoReunion data)
        {
            return await _repository.GuardarTipoReunion(data);
        }

        public async Task<int> ActualizarTipoReunion(TipoReunion data)
        {
            return await _repository.ActualizarTipoReunion(data);
        }

        public async Task<PagedResult<TipoReunion>> ConsultarTipoReunionIndicador(TipoReunionIndicadorFiltro data)
        {
            return await _repository.ConsultarTipoReunionIndicador(data);
        }
    }
}

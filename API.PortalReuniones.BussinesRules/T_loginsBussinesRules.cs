﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Paged;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace API.PortalReuniones.BussinesRules
{
    public class T_loginsBussinesRules
    {
        private readonly IT_loginsRepositories _repository;
        readonly PortalReunionesContext db;
        public T_loginsBussinesRules(IT_loginsRepositories repository)
        {
            _repository = repository;
        }

        public async Task<PagedResult<TLogins>> ConsultarT_logins(TLoginsFiltro data)
        {
            return await _repository.ConsultarT_logins(data);
        }

        public async Task<PagedResult<TLogins>> ConsultarT_loginsFiltro(PresentarT_loginsFiltro data)
        {
            return await _repository.ConsultarT_loginsFiltro(data);
        }

        public async Task<List<TLogins>> ConsultarT_loginsActa(int id_acta)
        {
            List<TLogins> model = new List<TLogins>();
            try
            {
                string query = "SELECT     t_l.nombreusuario, t_l.Login " +
                         "FROM         (PR.Acta_Participante AP INNER JOIN " +
                         "PR.t_logins t_l ON AP.login = t_l.Login AND AP.login = t_l.Login)" +
                         "WHERE     AP.id_acta =" + id_acta +"";
                await db.Database.ExecuteSqlRawAsync(query, model);
            }
            catch (Exception ex)
            {

                throw;
            }
            return model;
        }
    }
}

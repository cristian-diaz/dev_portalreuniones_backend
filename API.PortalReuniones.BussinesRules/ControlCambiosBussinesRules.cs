﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Paged;

namespace API.PortalReuniones.BussinesRules
{
    public class ControlCambiosBussinesRules
    {
        private readonly IControlCambiosRepositories _repository;

        public ControlCambiosBussinesRules(IControlCambiosRepositories repository)
        {
            _repository = repository;
        }


        public async Task<PagedResult<ControlCambios>> ConsultarControlCambios(ControlCambiosFiltro data)
        {
            return await _repository.ConsultarControlCambios(data);
        }

        public async Task<int> ActualizarControlCambios(ControlCambios viewModel)
        {
            return await _repository.ActualizarControlCambios(viewModel);
        }
    }
}

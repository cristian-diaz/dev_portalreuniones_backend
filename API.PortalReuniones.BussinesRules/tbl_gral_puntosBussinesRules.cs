﻿using API.PortalReuniones.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.PortalReuniones.BussinesRules
{
    public class tbl_gral_puntosBussinesRules
    {
        readonly PortalReunionesContext db;

        public tbl_gral_puntosBussinesRules(PortalReunionesContext context)
        {
            db = context;
        }

        public async Task<TblGralPuntos> Consultartbl_gral_puntos(int id_punto)
        {
            TblGralPuntos model = new TblGralPuntos();
            model = await Task.Run(() => {
                var t_gp = db.tbl_gral_puntos.Where(c => c.IdPunto == id_punto).FirstOrDefault();
                return t_gp;
            });
            return model;
        }

        public async Task<TblGralPuntos> Guardartbl_gral_puntos(TblGralPuntos viewModel)
        {
            await db.tbl_gral_puntos.AddAsync(viewModel);
            await db.SaveChangesAsync();
            return viewModel;
        }
        public async Task<TblGralPuntos> Actualizartbl_gral_puntos(TblGralPuntos viewModel)
        {
            var dato = db.tbl_gral_puntos.Where(m => m.IdPunto == viewModel.IdPunto && m.IdPuntosReunion == viewModel.IdPuntosReunion).FirstOrDefault();
            if (viewModel.IdPunto > 0)
                dato.IdPunto = viewModel.IdPunto;
            if (viewModel.IdPuntosReunion > 0)
                dato.IdPuntosReunion = viewModel.IdPuntosReunion;
            if (viewModel.Punto != null)
                dato.Punto = viewModel.Punto;
            if (viewModel.Responsable != null)
                dato.Responsable = viewModel.Responsable;

            db.Entry(dato).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return dato;
        }

        public async Task<int> Borrartbl_gral_puntos(TblGralPuntos viewModel)
        {
            db.tbl_gral_puntos.Remove(viewModel);
            return await db.SaveChangesAsync();
        }

        public async Task<List<TblGralPuntos>> Consultartbl_gral_puntos(string strWhere)
        {
            List<TblGralPuntos> model = new List<TblGralPuntos>();
            try
            {
                string query = "SELECT     tbl_gral_puntos.id_puntos_reunion, tbl_gral_puntos.punto, tbl_gral_puntos.responsable " +
                                   " FROM         (tbl_gral_puntos INNER JOIN " +
                                   " tbl_pntos_rnion ON tbl_gral_puntos.id_punto = tbl_pntos_rnion.Id_puntos) " +
                                   " WHERE     " + strWhere;
                await db.Database.ExecuteSqlRawAsync(query, model);
            }
            catch (Exception ex)
            {

                throw;
            }
            return model;
        }
    }
}

﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Interfaces;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Paged;
using API.PortalReuniones.Entities.EntitiesExtension;

namespace API.PortalReuniones.BussinesRules
{
    public class CompromisosBussinesRules
    {
        private readonly ICompromisosRepositories _repository;

        public CompromisosBussinesRules(ICompromisosRepositories repository)
        {
            _repository = repository;
        }

        public async Task<int> GuardarCompromiso(Compromisos data)
        {
            return await _repository.GuardarCompromiso(data);
        }

        public async Task<int> ActualizarCompromiso(Compromisos data)
        {
            return await _repository.ActualizarCompromisos(data);
        }

        public async Task<PagedResult<Compromisos>> ConsultarCompromiso(CompromisosFiltro data)
        {
            return await _repository.ConsultarCompromiso(data);
        }

        public int PaginadoTotal(List<Compromisos> compromisos, int paginado)
        {
            int cantidad = compromisos.Count();
            return Convert.ToInt32(Math.Ceiling((decimal)cantidad / (decimal)paginado));
        }

        public async Task<int> BorrarCompromiso(Compromisos viewModel)
        {
            //db.Compromisos.Remove(viewModel);
            return 1;
        }

        public async Task<PagedResult<Compromisos>> ConsultarCompromisoFiltros(PresentarCompromisosFiltro data)
        {
            return await _repository.ConsultarCompromisoFiltros(data);
        }

        public async Task<PagedResult<Compromisos>> ConsultarCompromisoActaFiltros(CompromisosActaFiltro data)
        {
            return await _repository.ConsultarCompromisoActaFiltros(data);
        }

        public async Task<PagedResult<ReporteCompromisos>> ConsultarReporteCompromisos(ReporteCompromisosFiltro data)
        {
            if (data.Page == 0)
            {
                data.Page = 1;
            }

            if (data.Size == 0)
            {
                data.Size = 1000;
            }
            return await _repository.ConsultarReporteCompromisos(data);
        }
    }
}

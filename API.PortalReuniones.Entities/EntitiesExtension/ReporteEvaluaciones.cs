﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.EntitiesExtension
{
    public class ReporteEvaluaciones
    {
        public string Nombre { get; set; }
        public int? TipAsoc { get; set; }
        public string Item { get; set; }
        public string TipoNm { get; set; }
        public string Calificacion { get; set; }
        public string Permiso { get; set; }
        public int? TotalPuntos { get; set; }
        public string Login { get; set; }
        public int? IdItem { get; set; }
        public string NombreUsuario { get; set; }
        public DateTime? Fecha { get; set; }
        public string Lugar { get; set; }
        public string Comentario { get; set; }
        public DateTime? HoraInicioReal { get; set; }
        public DateTime? HoraFinalReal { get; set; }
        public DateTime? HoraEstimadaInicio { get; set; }
        public DateTime? HoraEstimadaFinal { get; set; }
    }
}

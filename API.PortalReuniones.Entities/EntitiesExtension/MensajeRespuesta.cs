﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.EntitiesExtension
{
    public class MensajeRespuesta
    {
        public int Codigo { get; set; }
        public int Mensaje { get; set; }
    }
}

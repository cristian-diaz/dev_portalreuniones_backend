﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace API.PortalReuniones.Entities.EntitiesExtension
{
    public class CompromisosActa_Compromisos
    {
        public int NoID { get; set; }

        [StringLength(200)]
        public string TIPO { get; set; }

        [StringLength(50)]
        public string NOMBRE { get; set; }

        [StringLength(70)]
        public string TEMA { get; set; }

        public string Accion { get; set; }

        [StringLength(70)]
        public string RESPONSABLE { get; set; }

        public DateTime? FECHAI { get; set; }

        public DateTime? FECHAC { get; set; }

        [StringLength(50)]
        public string STATUS { get; set; }

        [StringLength(50)]
        public string LOGIN { get; set; }

        public string SOLUCION { get; set; }

        public string ARCHIVO_ASOCIADO { get; set; }

        public bool VERIFICADO { get; set; }

        public DateTime? FechaRealCierre { get; set; }

        public DateTime? HoraCierre { get; set; }

        public string PRIORIZACION { get; set; }

        public string CATEGORIA { get; set; }

        public int? Id_Periodicidad { get; set; }

        public DateTime? Fecha_EnvioMail { get; set; }

        [StringLength(10)]
        public string Delegado { get; set; }

        public DateTime? fecha_reunion { get; set; }

        public DateTime? hora_reunion { get; set; }

        [StringLength(255)]
        public string Usuario_registra { get; set; }



        public int id_acta { get; set; }
    }
}

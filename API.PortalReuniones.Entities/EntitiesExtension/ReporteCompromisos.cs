﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.EntitiesExtension
{
    public class ReporteCompromisos
    {
        public int NoId { get; set; }

        public string Tipo { get; set; }

        public string Nombre { get; set; }

        public string Tema { get; set; }

        public string Accion { get; set; }

        public string Responsable { get; set; }

        public DateTime? FechaI { get; set; }

        public DateTime? FechaC { get; set; }

        public string Status { get; set; }

        public string Login { get; set; }

        public string Solucion { get; set; }

        public string ArchivoAsociado { get; set; }

        public bool Verificado { get; set; }

        public DateTime? FechaRealCierre { get; set; }

        public DateTime? HoraCierre { get; set; }

        public string Priorizacion { get; set; }

        public string Categoria { get; set; }

        public string IdPeriodicidad { get; set; }

        public DateTime? FechaEnvioMail { get; set; }

        public string Delegado { get; set; }

        public DateTime? FechaReunion { get; set; }

        public DateTime? HoraReunion { get; set; }

        public string UsuarioRegistra { get; set; }

        public int State { get; set; }

        //Campo adicionales
        public string NombreUsuario { get; set; }

    }
}

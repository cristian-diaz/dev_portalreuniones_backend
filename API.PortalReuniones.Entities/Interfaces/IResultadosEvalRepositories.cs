﻿using API.PortalReuniones.Entities.EntitiesExtension;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Paged;
using System.Threading.Tasks;

namespace API.PortalReuniones.Entities.Interfaces
{
    public interface IResultadosEvalRepositories
    {
        public Task<PagedResult<ReporteEvaluaciones>> ConsultarReporteEvaluacion(ReporteEvaluacionFiltro data);
    }
}

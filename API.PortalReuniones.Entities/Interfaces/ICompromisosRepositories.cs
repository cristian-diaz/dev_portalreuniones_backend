﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.EntitiesExtension;
using API.PortalReuniones.Entities.Filtro;
using System.Threading.Tasks;
using API.PortalReuniones.Entities.Paged;

namespace API.PortalReuniones.Entities.Interfaces
{
    public interface ICompromisosRepositories
    {
        public Task<int> GuardarCompromiso(Compromisos data);
        public Task<PagedResult<Compromisos>> ConsultarCompromiso(CompromisosFiltro data);
        public Task<PagedResult<Compromisos>> ConsultarCompromisoFiltros(PresentarCompromisosFiltro data);
        public Task<PagedResult<Compromisos>> ConsultarCompromisoActaFiltros(CompromisosActaFiltro data);
        //public Task<int> BorrarCompromiso(Compromisos viewModel);
        public Task<int> ActualizarCompromisos(Compromisos data);
        //public Task<int> ConsultarCompromisoTipoReunionIncumplidos(string tipo);
        //public int PaginadoTotal(List<Compromisos> compromisos, int paginado);
        public Task<PagedResult<ReporteCompromisos>> ConsultarReporteCompromisos(ReporteCompromisosFiltro data);
    }
}
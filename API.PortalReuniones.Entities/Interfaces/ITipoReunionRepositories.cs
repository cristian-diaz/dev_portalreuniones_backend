﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Paged;
using System.Threading.Tasks;

namespace API.PortalReuniones.Entities.Interfaces
{
    public interface ITipoReunionRepositories
    {
        public Task<PagedResult<TipoReunion>> ConsultarTipoReunion(TipoReunionFiltro data);
        //Task<TipoReunion> ConsultarTipoReunion(string nombre);
        //Task<List<TipoReunion>> ConsultarTipoReunion();
        //Task<List<TipoReunion>> ConsultarTipoReunionTema();
        public Task<PagedResult<TipoReunion>> ConsultarTipoReunionFiltro(PresentarTipoReunionFiltro data);
        public Task<PagedResult<TipoReunion>> ConsultarTipoReunionIndicador(TipoReunionIndicadorFiltro data);
        public Task<int> GuardarTipoReunion(TipoReunion data);
        public Task<int> ActualizarTipoReunion(TipoReunion data);
    }
}

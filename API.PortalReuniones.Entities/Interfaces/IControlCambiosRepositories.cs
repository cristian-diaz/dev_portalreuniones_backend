﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Paged;
using System.Threading.Tasks;

namespace API.PortalReuniones.Entities.Interfaces
{
    public interface IControlCambiosRepositories
    {
        public Task<PagedResult<ControlCambios>> ConsultarControlCambios(ControlCambiosFiltro data);
        public Task<int> ActualizarControlCambios(ControlCambios viewModel);
    }
}
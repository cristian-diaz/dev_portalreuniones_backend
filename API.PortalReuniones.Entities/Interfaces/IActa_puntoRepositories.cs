﻿using API.PortalReuniones.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API.PortalReuniones.Entities.Interfaces
{
    public interface IActa_puntoRepositories
    {
        public Task<ActaPunto> GuardarActa_punto(ActaPunto viewModel);
        public Task<ActaPunto> ActualizarActa_punto(ActaPunto viewModel);
    }
}

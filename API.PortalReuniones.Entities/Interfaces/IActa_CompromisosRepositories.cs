﻿using API.PortalReuniones.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API.PortalReuniones.Entities.Interfaces
{
    public interface IActa_CompromisosRepositories
    {
        public Task<int> GuardarActa_Compromisos(ActaCompromisos data);
        public Task<int> ActualizarActa_Compromisos(ActaCompromisos data);
    }
}

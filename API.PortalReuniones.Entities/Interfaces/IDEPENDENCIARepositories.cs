﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Paged;
using System.Threading.Tasks;

namespace API.PortalReuniones.Entities.Interfaces
{
    public interface IDependenciaRepositories
    {
        public Task<PagedResult<Dependencia>> GetDepencias();
        public Task<PagedResult<Dependencia>> ConsultarDEPENDENCIA(DEPENDENCIAFiltro data);
    }
}

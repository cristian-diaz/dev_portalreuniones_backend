﻿using API.PortalReuniones.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API.PortalReuniones.Entities.Interfaces
{
    public interface IActa_ParticipanteRepositories
    {
        public Task<ActaParticipante> GuardarActa_Participante(ActaParticipante viewModel);
        public Task<ActaParticipante> ActualizarActa_Participante(ActaParticipante viewModel);
    }
}

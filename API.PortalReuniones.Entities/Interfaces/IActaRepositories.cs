﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Paged;
using System.Threading.Tasks;

namespace API.PortalReuniones.Entities.Interfaces
{
    public interface IActaRepositories
    {
        public Task<int> GuardarActa(Acta data);
        public Task<PagedResult<Acta>> ConsultarActa(ActaFiltro data);
        public Task<int> ActualizarActa(Acta data);

    }
}

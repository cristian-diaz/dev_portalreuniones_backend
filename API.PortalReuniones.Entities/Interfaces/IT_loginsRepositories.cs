﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Paged;
using System.Threading.Tasks;

namespace API.PortalReuniones.Entities.Interfaces
{
    public interface IT_loginsRepositories
    {
        public Task<PagedResult<TLogins>> ConsultarT_logins(TLoginsFiltro data);
        public Task<PagedResult<TLogins>> ConsultarT_loginsFiltro(PresentarT_loginsFiltro data);
    }
}

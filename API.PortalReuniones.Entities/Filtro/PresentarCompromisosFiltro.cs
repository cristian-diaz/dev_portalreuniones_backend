﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class PresentarCompromisosFiltro
    {
        public bool Administrador { get; set; }
        public string Login { get; set; }
        public string Reunion { get; set; }
        public string Status { get; set; }
        public bool Verificado { get; set; }
        public int NoID { get; set; }
        public string Tema { get; set; }
        public string Responsable { get; set; }
        public string Delegado { get; set; }
        public string CondicionFI { get; set; }
        public DateTime? FechaI { get; set; }
        public string CondicionFC { get; set; }
        public DateTime? FechaC { get; set; }
        public short NoDias { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
        public int State { get; set; }
    }
}

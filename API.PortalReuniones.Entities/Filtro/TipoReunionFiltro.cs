﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class TipoReunionFiltro
    {
        public int? Id { get; set; }
        public string Nombre { get; set; }
        public decimal? Activa { get; set; }
        public string TipoNm { get; set; }
        public int? TipAsoc { get; set; }
        public DateTime? HoraInicio { get; set; }
        public DateTime? HoraFin { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
    }
}

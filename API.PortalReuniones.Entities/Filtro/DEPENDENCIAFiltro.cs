﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class DEPENDENCIAFiltro
    {
        public string DependenciaId { get; set; }
        public int TipoDepId { get; set; }
        public string DepPadreId { get; set; }
        public string DependenciaNombre { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
    }
}

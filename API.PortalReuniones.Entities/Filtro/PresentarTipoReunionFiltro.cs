﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class PresentarTipoReunionFiltro
    {
        public string Login { get; set; }
        public string TipoNm { get; set; }
        public string Permiso { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
    }
}

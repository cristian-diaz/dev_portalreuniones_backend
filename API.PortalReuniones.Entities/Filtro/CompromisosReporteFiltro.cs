﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class CompromisosReporteFiltro
    {
        public string TEMA { get; set; }
        public string Accion { get; set; }
        public string TIPO { get; set; }
        public string RESPONSABLE { get; set; }
    }
}

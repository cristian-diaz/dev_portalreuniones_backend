﻿using System;

namespace API.PortalReuniones.Entities.Filtro
{
    public class ActaFiltro
    {
        public int IdActa { get; set; }
        public int NumActa { get; set; }
        public DateTime? FechaActa { get; set; }
        public int Estado { get; set; }
        public string NombreActa { get; set; }
        public string TemaActa { get; set; }
        public DateTime? FechaEval { get; set; }
        public DateTime? HoraInicio { get; set; }
        public DateTime? HoraFin { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
    }
}

﻿
namespace API.PortalReuniones.Entities.Filtro
{
    public class ReunionArchivosFiltro
    {
        public int? Id { get; set; }
        public int? Reunion { get; set; }
        public string Nombre { get; set; }
        public string Ruta { get; set; }
        public string Usuario { get; set; }
        public string TpDocumento { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
    }
}

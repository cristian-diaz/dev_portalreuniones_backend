﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class ItemEvaluacionFiltro
    {
        public int? IdItem { get; set; }
        public string Item { get; set; }
        public int? Peso { get; set; }
        public string Comentario { get; set; }
        public string Calificacion { get; set; }
        public int? MaxPuntos { get; set; }
        public bool? Activo { get; set; }
        public bool? Calc { get; set; }
        public int? Ord { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
    }
}

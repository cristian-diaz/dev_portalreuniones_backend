﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class TLoginsFiltro
    {
        public string Login { get; set; }

        public bool? Grupo { get; set; }

        public string NombreUsuario { get; set; }

        public int? Rol { get; set; }

        public int Administrador { get; set; }

        public string EMail { get; set; }

        public bool? Activo { get; set; }

        public int Page { get; set; }

        public int Size { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class TipoReunionIndicadorFiltro
    {
        public string tipo { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
    }
}

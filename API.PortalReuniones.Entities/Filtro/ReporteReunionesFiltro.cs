﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class ReporteReunionesFiltro
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public decimal? Activa { get; set; }
        public string tipo_nm { get; set; }
        public int? tip_asoc { get; set; }
        public DateTime? horaInicio { get; set; }
        public DateTime? horaFin { get; set; }
    }
}

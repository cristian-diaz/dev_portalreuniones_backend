﻿
namespace API.PortalReuniones.Entities.Filtro
{
    public class CompromisosActaFiltro
    {
        public int? IdActa { get; set; }
        public int Pagina { get; set; }
        public int Size { get; set; }
        public int State { get; set; }
    }
}

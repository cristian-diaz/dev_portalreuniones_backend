﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class ControlCambiosFiltro
    {
        public int? NoId { get; set; }
        public int? CompromisoId { get; set; }
        public string Tipo { get; set; }
        public string Tema { get; set; }
        public string Accion { get; set; }
        public string Responsable { get; set; }
        public DateTime? FechaI { get; set; }
        public DateTime? FechaC { get; set; }
        public string Status { get; set; }
        public string Login { get; set; }
        public string Solucion { get; set; }
        public string ArchivoAsociado { get; set; }
        public bool? Verificado { get; set; }
        public DateTime? FechaRealCierre { get; set; }
        public DateTime? FechaCambio { get; set; }
        public string MotivoCambio { get; set; }
        public string NuevoResponsable { get; set; }
        public string ModificadoPor { get; set; }
        public double? TipoDeCambio { get; set; }
        public DateTime? HoraCierre { get; set; }
        public string NombreNuevoResp { get; set; }
        public string Priorizacion { get; set; }
        public string Categoria { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class ReporteEvaluacionFiltro
    {
        public string TipoNm { get; set; }
        public string Permiso { get; set; }
        public string Nombre { get; set; }
        public int? IdItem { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public DateTime? HoraInicioReal { get; set; }
        public DateTime? HoraFinReal { get; set; }
        public int? TotalPuntos { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }

    }
}

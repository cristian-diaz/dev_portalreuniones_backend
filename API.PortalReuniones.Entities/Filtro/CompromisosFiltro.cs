﻿using System;

namespace API.PortalReuniones.Entities.Filtro
{
    public class CompromisosFiltro
    {
        public int? NoID { get; set; }
        public string Tipo { get; set; }
        public DateTime? FechaR { get; set; }
        public string UsuarioRegistra { get; set; }
        public int Pagina { get; set; }
        public int Size { get; set; }
        public string Nombre { get; set; }
        public string Tema { get; set; }
        public string Responsable { get; set; }
        public DateTime? FechaI { get; set; }
        public string Status { get; set; }
        public string Delegado { get; set; }
        public string OperatorFR{ get; set; }
        public string OperatorFI{ get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class ReporteCompromisosFiltro
    {
        public string tema { get; set; }
        public string accion { get; set; }
        public string tipo { get; set; }
        public string responsable { get; set; }
        public int state { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
    }
}

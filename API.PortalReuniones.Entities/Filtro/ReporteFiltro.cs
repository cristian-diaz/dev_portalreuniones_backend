﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class ReporteFiltro
    {
        public reporteCodigo reporteCodigo { get; set; }
        public TipoReunionFiltro reporteReunionesFiltro { get; set; }
        public ReporteCompromisosFiltro reporteCompromisosFiltro { get; set; }
        public ReporteEvaluacionFiltro reporteEvaluacionFiltro { get; set; }
    }

    public enum reporteCodigo : ushort
    { 
        Listado_Reuniones = 0,
        Listado_Compromisos = 1,
        Listado_Evaluaciones = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.PortalReuniones.Entities.Filtro
{
    public class PresentarT_loginsFiltro
    {
        public int id_reunion { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
    }
}

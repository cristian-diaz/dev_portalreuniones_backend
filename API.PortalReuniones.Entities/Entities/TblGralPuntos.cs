﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class TblGralPuntos
    {
        [Key]
        [Column("id_puntos_reunion")]
        public int IdPuntosReunion { get; set; }

        [Column("id_punto")]
        public int? IdPunto { get; set; }

        [Column("punto")]
        public string Punto { get; set; }

        [StringLength(255)]
        [Column("responsable")]
        public string Responsable { get; set; }
    }
}

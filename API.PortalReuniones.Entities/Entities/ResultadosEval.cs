﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class ResultadosEval
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("No_Eval")]
        public int NoEval { get; set; }

        [Column("Id")]
        public int? Id { get; set; }

        [StringLength(50)]
        [Column("login")]
        public string Login { get; set; }

        [StringLength(200)]
        [Column("lugar")]
        public string Lugar { get; set; }

        [Column("fecha")]
        public DateTime? Fecha { get; set; }

        [Column("totalpuntos")]
        public int? TotalPuntos { get; set; }

        [Column("porcentaje")]
        public int? Porcentaje { get; set; }

        [Column("Cant_evareun")]
        public short? CantEvaReun { get; set; }

        [Column("Comentario")]
        public string Comentario { get; set; }

        [Column("hora_inici_real")]
        public DateTime? HoraInicioReal { get; set; }

        [Column("hora_final_real")]
        public DateTime? HoraFinalReal { get; set; }

        [Column("hora_estimada_inicio")]
        public DateTime? HoraEstimadaInicio { get; set; }

        [Column("hora_estimada_final")]
        public DateTime? HoraEstimadaFinal { get; set; }
    }
}

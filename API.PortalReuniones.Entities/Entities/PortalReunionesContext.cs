﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace API.PortalReuniones.Entities.Entities
{
    public class PortalReunionesContext : DbContext
    {
        public PortalReunionesContext(DbContextOptions<PortalReunionesContext> options)
            : base(options)
        {
        }
        
        public virtual DbSet<Acta> Acta { get; set; }
        public virtual DbSet<ActaCompromisos> Acta_Compromisos { get; set; }
        public virtual DbSet<ActaParticipante> Acta_Participante { get; set; }
        public virtual DbSet<ActaPunto> Acta_punto { get; set; }
        public virtual DbSet<AsistentesReunion> AsistentesReunion { get; set; }
        public virtual DbSet<Compromisos> Compromisos { get; set; }
        public virtual DbSet<CalFestivos> Cal_Festivos { get; set; }
        public virtual DbSet<CompEliminados> CompEliminados { get; set; }
        public virtual DbSet<ControlCambios> ControlCambios { get; set; }
        public virtual DbSet<Dependencia> DEPENDENCIA { get; set; }
        public virtual DbSet<ItemEvaluacion> Item_Evaluacion { get; set; }
        public virtual DbSet<Jerarquia> Jerarquia { get; set; }
        public virtual DbSet<LoginGrupo> login_grupo { get; set; }
        public virtual DbSet<Permisos> permisos { get; set; }
        public virtual DbSet<ResultadosCalif> Resultados_Calif { get; set; }
        public virtual DbSet<ResultadosEval> Resultados_Eval { get; set; }
        public virtual DbSet<ReunionArchivos> Reunion_Archivos { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<TLogins> t_logins { get; set; }
        public virtual DbSet<TReuniones> T_REUNIONES { get; set; }
        public virtual DbSet<TblGralPuntos> tbl_gral_puntos { get; set; }
        public virtual DbSet<TblPntosRnion> tbl_pntos_rnion { get; set; }
        public virtual DbSet<TipoReunion> TipoReunion { get; set; }
        public virtual DbSet<Periodicidad> Periodicidad { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Compromisos>()
                .Property(e => e.Accion)
                .IsUnicode(false);

            modelBuilder.Entity<Compromisos>()
                .Property(e => e.Priorizacion)
                .IsUnicode(false);

            modelBuilder.Entity<CompEliminados>()
                .Property(e => e.Responsable)
                .IsUnicode(false);

            modelBuilder.Entity<CompEliminados>()
                .Property(e => e.BorradoPor)
                .IsUnicode(false);

            modelBuilder.Entity<CompEliminados>()
                .Property(e => e.Priorizacion)
                .IsUnicode(false);

            modelBuilder.Entity<TipoReunion>()
                .Property(e => e.Activa)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Compromisos>()
                .HasKey(c => new { c.NoId });

            modelBuilder.Entity<CompEliminados>()
                .HasKey(c => new { c.NoId });

            modelBuilder.Entity<ControlCambios>()
                .HasKey(c => new { c.NoId });

            modelBuilder.Entity<Dependencia>()
                .HasKey(c => new { c.DependenciaId });

            modelBuilder.Entity<Dependencia>()
             .ToTable("DEPENDENCIA", "PR");

            modelBuilder.Entity<ItemEvaluacion>()
                .HasKey(c => new { c.IdItem, c.Activo, c.Calc });

            modelBuilder.Entity<LoginGrupo>()
                .HasKey(c => new { c.LoginGrupoId, c.LoginGrupoGrupoId, c.LoginGrupoLoginId });

            modelBuilder.Entity<Permisos>()
                .HasKey(c => new { c.IdReunion });

            modelBuilder.Entity<ResultadosEval>()
               .HasKey(c => new { c.NoEval });

            modelBuilder.Entity<ResultadosCalif>()
              .HasKey(c => new { c.NoEval, c.Id, c.IdItem });

            modelBuilder.Entity<Roles>()
               .HasKey(c => new { c.Id, c.Lectura, c.Escritura, c.UpDel });

            modelBuilder.Entity<TLogins>()
               .HasKey(c => new { c.Grupo, c.Administrador });

            modelBuilder.Entity<TblGralPuntos>()
               .HasKey(c => new { c.IdPuntosReunion });

            modelBuilder.Entity<TblPntosRnion>()
               .HasKey(c => new { c.IdPuntos });

            modelBuilder.Entity<TipoReunion>()
               .HasKey(c => new { c.Id });
            
            modelBuilder.Entity<Compromisos>()
                .ToTable("Compromisos", "PR");

            modelBuilder.Entity<TipoReunion>()
                .ToTable("TipoReunion", "PR");

            modelBuilder.Entity<Acta>()
                .HasKey(c => new { c.IdActa });

            modelBuilder.Entity<Acta>()
              .ToTable("Acta", "PR");

            modelBuilder.Entity<ActaCompromisos>()
                .HasKey(e => e.IdActa);

            modelBuilder.Entity<ActaCompromisos>()
                .HasKey(e => e.IdCompromisos);

            modelBuilder.Entity<ActaCompromisos>()
              .ToTable("Acta_Compromisos", "PR");

            modelBuilder.Entity<ActaParticipante>()
                .HasKey(e => e.IdActa);

            modelBuilder.Entity<ActaParticipante>()
                .HasKey(e => e.Login);

            modelBuilder.Entity<ActaParticipante>()
              .ToTable("Acta_Participante", "PR");

            modelBuilder.Entity<ActaPunto>()
              .HasKey(e => e.IdActa);

            modelBuilder.Entity<ActaPunto>()
                .HasKey(e => e.IdPuntosReunion);

            modelBuilder.Entity<ActaPunto>()
                .HasKey(e => e.IdPunto);

            modelBuilder.Entity<ActaPunto>()
              .ToTable("Acta_punto", "PR");

            modelBuilder.Entity<AsistentesReunion>()
               .HasKey(e => e.IdReunion);

            modelBuilder.Entity<AsistentesReunion>()
              .ToTable("AsistentesReunion", "PR");
            
            modelBuilder.Entity<TLogins>()
              .ToTable("t_logins", "PR");

            modelBuilder.Entity<Periodicidad>()
              .ToTable("Periodicidad", "PR");

            modelBuilder.Entity<Permisos>()
              .ToTable("permisos", "PR");

            modelBuilder.Entity<ResultadosCalif>()
              .ToTable("Resultados_Calif", "PR");

            modelBuilder.Entity<ResultadosEval>()
            .ToTable("Resultados_Eval", "PR");

            modelBuilder.Entity<ItemEvaluacion>()
            .ToTable("Item_Evaluacion", "PR");

            modelBuilder.Entity<ReunionArchivos>()
           .ToTable("Reunion_Archivos", "PR");

            modelBuilder.Entity<LoginGrupo>()
          .ToTable("login_grupo", "PR");

            modelBuilder.Entity<CalFestivos>()
         .ToTable("Cal_Festivos", "PR");

            modelBuilder.Entity<TReuniones>()
         .ToTable("T_REUNIONES", "PR");

            modelBuilder.Entity<TblGralPuntos>()
         .ToTable("tbl_gral_puntos", "PR");

            modelBuilder.Entity<TblPntosRnion>()
         .ToTable("tbl_pntos_rnion", "PR");

            base.OnModelCreating(modelBuilder);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace API.PortalReuniones.Entities.Entities
{
    [Table("PR.Roles")]
    public partial class Roles
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("Id")]
        public int Id { get; set; }

        [StringLength(255)]
        [Column("Rol")]
        public string Rol { get; set; }

        [Key]
        [Column("Lectura")]
        public bool Lectura { get; set; }

        [Key]
        [Column("escritura")]
        public bool Escritura { get; set; }

        [Key]
        [Column("UpDel")]
        public bool UpDel { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public class Periodicidad
    { 
        [Key]
        [StringLength(100)]
        [Column("Id_Periodicidad")]
        public string IdPeriodicidad { get; set; }

        [StringLength(100)]
        [Column("Descripcion")]
        public string Descripcion { get; set; }

        [Column("No_Dias")]
        public Int16 NoDias { get; set; }
    }
}

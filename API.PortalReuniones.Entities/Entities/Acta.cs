﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public class Acta
    {
        [Key]
        [Column("id_acta")]
        public int IdActa { get; set; }

        [Column("num_acta")]
        public int NumActa { get; set; }

        [Column("fecha_acta")]
        public DateTime FechaActa { get; set; }

        [Column("estado")]
        public int Estado { get; set; }

        [Column("codigo_formato")]
        public string CodigoFormato { get; set; }

        [Column("version")]
        public string Version { get; set; }

        [Column("nombre_acta")]
        public string NombreActa { get; set; }

        [Column("Departamento_acta")]
        public string DepartamentoActa { get; set; }

        [Column("tema_acta")]
        public string TemaActa { get; set; }

        [Column("ubicacion")]
        public string Ubicacion { get; set; }

        [Column("Fecha_eval")]
        public DateTime FechaEval { get; set; }

        [Column("hora_inicio")]
        public DateTime HoraInicio { get; set; }

        [Column("hora_fin")]
        public DateTime HoraFin { get; set; }

        public string Objetivo { get; set; }

        [Column("Eval_reunion")]
        public int EvalReunion { get; set; }

        [Column("asistentes")]
        public string Asistentes { get; set; }

        [Column("reviso")]
        public string Reviso { get; set; }

        [Column("Aprobo")]
        public string Aprobo { get; set; }

        [Column("correo_aprobo")]
        public string CorreoAprobo { get; set; }
    }
}

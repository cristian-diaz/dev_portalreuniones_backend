﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public class ActaCompromisos
    {
        [Key]
        [Column("id_acta")]
        public int IdActa { get; set; }

        [Key]
        [Column("id_compromisos")]
        public int IdCompromisos { get; set; }
    }
}

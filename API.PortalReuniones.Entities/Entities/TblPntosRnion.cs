﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class TblPntosRnion
    {
        [Key]
        [Column("Id_puntos")]
        public int IdPuntos { get; set; }

        [Column("id_reunion")]
        public int? IdReunion { get; set; }

        [Column("fecha")]
        public DateTime? Fecha { get; set; }

        [Column("hora")]
        public DateTime? Hora { get; set; }

        [Column("agenda")]
        public string Agenda { get; set; }

        [Column("No_eval")]
        public int? NoEval { get; set; }
    }
}

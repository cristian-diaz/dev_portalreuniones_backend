﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class LoginGrupo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("login_grupo_id")]
        public int LoginGrupoId { get; set; }

        [Key]
        [StringLength(30)]
        [Column("login_grupo_grupoid")]
        public string LoginGrupoGrupoId { get; set; }

        [Key]
        [StringLength(30)]
        [Column("login_grupo_loginid")]
        public string LoginGrupoLoginId { get; set; }
    }
}

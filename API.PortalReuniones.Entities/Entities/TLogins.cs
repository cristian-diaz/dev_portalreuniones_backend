﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class TLogins
    {
        [StringLength(30)]
        [Column("Login")]
        public string Login { get; set; }

        [Key]
        [Column("grupo")]
        public bool? Grupo { get; set; }

        [StringLength(100)]
        [Column("nombreusuario")]
        public string NombreUsuario { get; set; }

        [Column("Rol")]
        public int? Rol { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("administrador")]
        public int Administrador { get; set; }

        [StringLength(100)]
        [Column("EMail")]
        public string EMail { get; set; }

        [Column("Activo")]
        public bool? Activo { get; set; }

        [NotMapped]
        [Column("id_reunion")]
        public int IdReunion { get; set; }
    }
}

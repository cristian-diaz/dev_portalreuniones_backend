﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace API.PortalReuniones.Entities.Entities
{
    public class ResultadosCalif
    {
        [Key]
        [Column("No_eval")]
        public int NoEval { get; set; }
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        [Key]
        [Column("Id_Item")]
        public int IdItem { get; set; }

        [Column("calificacion")]
        public string Calificacion  { get; set; }
    }
}

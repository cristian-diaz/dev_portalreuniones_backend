﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public class ActaPunto
    {
        [Key]
        [Column("id_acta")]
        public int IdActa { get; set; }

        [Key]
        [Column("id_puntos_reunion")]
        public int IdPuntosReunion { get; set; }

        [Key]
        [Column("id_punto")]
        public int IdPunto { get; set; }
    }
}

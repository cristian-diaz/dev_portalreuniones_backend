﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class TipoReunion
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [StringLength(200)]
        [Column("Nombre")]
        public string Nombre { get; set; }

        [Column("FechaInicio")]
        public DateTime? FechaInicio { get; set; }

        [Column("FechaFinaliza")]
        public DateTime? FechaFinaliza { get; set; }

        [Column("FechaIngreso")]
        public DateTime? FechaIngreso { get; set; }

        [Column("area")]
        public int? Area { get; set; }

        [StringLength(255)]
        [Column("moderador")]
        public string Moderador { get; set; }

        [Column("id_recurso")]
        public int? IdRecurso { get; set; }

        [Column("tip_asoc")]
        public int? TipAsoc { get; set; }

        [StringLength(50)]
        [Column("tipo_nm")]
        public string TipoNm { get; set; }

        [Column("Activa", TypeName = "numeric")]
        public decimal Activa { get; set; }

        [Column("horaInicio")]
        public DateTime? HoraInicio { get; set; }

        [Column("horaFin")]
        public DateTime? HoraFin { get; set; }

        [StringLength(30)]
        [Column("duracion")]
        public string Duracion { get; set; }

        [Column("validarAsistentes")]
        public bool ValidarAsistentes { get; set; }

        [Column("seguimiento")]
        public int? Seguimiento { get; set; }

        [NotMapped]
        public string Login { get; set; }

        [NotMapped]
        public string Permiso { get; set; }

        [NotMapped]
        public string Tipo { get; set; }
    }
}

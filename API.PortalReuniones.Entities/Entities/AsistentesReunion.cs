﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public class AsistentesReunion
    {
        [Key]
        [Column("idreunion")]
        public int IdReunion { get; set; }

        [Column("login")]
        public string Login { get; set; }

        [Column("fecha")]
        public DateTime Fecha { get; set; }

        [Column("HoraInicio")]
        public DateTime HoraInicio { get; set; }

        [Column("horaFin")]
        public string HoraFin { get; set; }

        [Column("puntual")]
        public string Puntual { get; set; }

        [Column("suplente")]
        public string Suplente { get; set; }

        [Column("excusa")]
        public string Excusa { get; set; }

        [Column("bandera")]
        public int Bandera { get; set; }

        [Column("No_Eval")]
        public int NoEval { get; set; }

        [Column("comentario")]
        public string Comentario { get; set; }
    }
}

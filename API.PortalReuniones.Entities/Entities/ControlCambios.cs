﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    [Table("PR.ControlCambios")]
    public partial class ControlCambios
    {
        [Key]
        [Column("NoID")]
        public int NoId { get; set; }

        [Key]
        [Column("CompromisoID")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CompromisoId { get; set; }

        [StringLength(200)]
        [Column("TIPO")]
        public string Tipo { get; set; }

        [StringLength(70)]
        [Column("TEMA")]
        public string Tema { get; set; }

        [Column("Accion")]
        public string Accion { get; set; }

        [StringLength(50)]
        [Column("RESPONSABLE")]
        public string Responsable { get; set; }

        [Column("FECHAI")]
        public DateTime? FechaI { get; set; }

        [Column("FECHAC")]
        public DateTime? FechaC { get; set; }

        [StringLength(50)]
        [Column("STATUS")]
        public string Status { get; set; }

        [StringLength(50)]
        [Column("LOGIN")]
        public string Login { get; set; }

        [Column("SOLUCION")]
        public string Solucion { get; set; }

        [StringLength(255)]
        [Column("ARCHIVO_ASOCIADO")]
        public string ArchivoAsociado { get; set; }

        [Key]
        [Column("VERIFICADO")]
        public bool Verificado { get; set; }

        [Column("FECHAREALCIERRE")]
        public DateTime? FechaRealCierre { get; set; }

        [Column("FECHACAMBIO")]
        public DateTime? FechaCambio { get; set; }

        [Column("MOTIVOCAMBIO")]
        public string MotivoCambio { get; set; }

        [StringLength(50)]
        [Column("NUEVORESPONSABLE")]
        public string NuevoResponsable { get; set; }

        [StringLength(50)]
        [Column("MODIFICADOPOR")]
        public string ModificadoPor { get; set; }

        [Column("TIPODECAMBIO")]
        public double? TipoDeCambio { get; set; }

        [Column("HoraCierre")]
        public DateTime? HoraCierre { get; set; }

        [StringLength(50)]
        [Column("NombreNUEVORESP")]
        public string NombreNuevoResp { get; set; }

        [StringLength(50)]
        [Column("PRIORIZACION")]
        public string Priorizacion { get; set; }

        [StringLength(50)]
        [Column("CATEGORIA")]
        public string Categoria { get; set; }
    }
}

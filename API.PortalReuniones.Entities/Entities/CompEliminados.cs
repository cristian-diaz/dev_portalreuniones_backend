﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    [Table("PR.CompEliminados")]
    public partial class CompEliminados
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("NoID")]
        public int NoId { get; set; }

        [StringLength(200)]
        [Column("TIPO")]
        public string Tipo { get; set; }

        [StringLength(200)]
        [Column("TEMA")]
        public string Tema { get; set; }

        [Column("Accion")]
        public string Accion { get; set; }

        [Column("RESPONSABLE")]
        public string Responsable { get; set; }

        [Column("FECHAI")]
        public DateTime? FechaI { get; set; }

        [Column("FECHAC")]
        public DateTime? FechaC { get; set; }

        [StringLength(50)]
        [Column("STATUS")]
        public string Status { get; set; }

        [StringLength(50)]
        [Column("LOGIN")]
        public string Login { get; set; }

        [Column("SOLUCION")]
        public string Solucion { get; set; }

        [Column("ARCHIVO_ASOCIADO")]
        public string ArchivoAsociado { get; set; }

        [Key]
        [Column("VERIFICADO")]
        public bool Verificado { get; set; }

        [Column("FechaRealCierre")]
        public DateTime? FechaRealCierre { get; set; }

        [Column("HoraCierre")]
        public DateTime? HoraCierre { get; set; }

        [Column("BorradoPor")]
        public string BorradoPor { get; set; }

        [Column("FechaBorrado")]
        public DateTime? FechaBorrado { get; set; }

        [Column("PRIORIZACION")]
        public string Priorizacion { get; set; }

        [Column("CATEGORIA")]
        public string Categoria { get; set; }

        [StringLength(255)]
        [Column("MOTIVO")]
        public string Motivo { get; set; }
    }
}

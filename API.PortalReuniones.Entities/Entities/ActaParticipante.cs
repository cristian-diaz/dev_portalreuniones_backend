﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public class ActaParticipante
    {
        [Key]
        [Column("id_acta")]
        public int IdActa { get; set; }

        [Key]
        [Column("login")]
        public string Login { get; set; }
    }
}

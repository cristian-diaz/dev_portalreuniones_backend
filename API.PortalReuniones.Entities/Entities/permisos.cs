﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class Permisos
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("id_reunion")]
        public int IdReunion { get; set; }

        [StringLength(30)]
        [Column("login")]
        public string Login { get; set; }

        [StringLength(20)]
        [Column("permiso")]
        public string Permiso { get; set; }
    }
}

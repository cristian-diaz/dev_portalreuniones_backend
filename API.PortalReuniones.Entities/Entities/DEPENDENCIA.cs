﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class Dependencia
    {
        [Key]
        [StringLength(70)]
        [Column("DEPENDENCIA_ID")]
        public string DependenciaId { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("TIPO_DEP_ID")]
        public int TipoDepId { get; set; }

        [StringLength(70)]
        [Column("DEP_PADRE_ID")]
        public string DepPadreId { get; set; }

        [StringLength(70)]
        [Column("DEPENDENCIA_NOMBRE")]
        public string DependenciaNombre { get; set; }
    }
}

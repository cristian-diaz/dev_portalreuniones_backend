﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class Compromisos
    {
        [Key]
        [Column("NoID")]
        public int NoId { get; set; }

        [Required]
        [StringLength(50)]
        [Column("TIPO")]
        public string Tipo { get; set; }

        [StringLength(250)]
        [Column("NOMBRE")]
        public string Nombre { get; set; }

        [StringLength(70)]
        [Column("TEMA")]
        public string Tema { get; set; }
      
        [Column("Accion")]
        public string Accion { get; set; }

        [StringLength(70)]
        [Column("RESPONSABLE")]
        public string Responsable { get; set; }

        [Column("FECHAI")]
        public DateTime? FechaI { get; set; }

        [Column("FECHAC")]
        public DateTime? FechaC { get; set; }

        [StringLength(50)]
        [Column("STATUS")]
        public string Status { get; set; }

        [StringLength(50)]
        [Column("LOGIN")]
        public string Login { get; set; }

        [Column("SOLUCION")]
        public string Solucion { get; set; }

        [Column("ARCHIVO_ASOCIADO")]
        public string ArchivoAsociado { get; set; }

        [Column("VERIFICADO")]
        public bool Verficado { get; set; }

        [Column("FechaRealCierre")]
        public DateTime? FechaRealCierre { get; set; }

        [Column("HoraCierre")]
        public DateTime? HoraCierre { get; set; }

        [Column("PRIORIZACION")]
        public string Priorizacion { get; set; }

        [Column("CATEGORIA")]
        public string Categoria { get; set; }

        [Column("Id_Periodicidad")]
        public string IdPeriodicidad { get; set; }

        [Column("Fecha_EnvioMail")]
        public DateTime? FechaEnvioMail { get; set; }

        [StringLength(10)]
        [Column("Delegado")]
        public string Delegado { get; set; }

        [Column("fecha_reunion")]
        public DateTime? FechaReunion { get; set; }

        [Column("hora_reunion")]
        public DateTime? HoraReunion { get; set; }

        [StringLength(255)]
        [Column("Usuario_registra")]
        public string UsuarioRegistra { get; set; }

        [Column("state")]
        public int State { get; set; } = 0;

        [NotMapped]
        public string Email { get; set; }

        [NotMapped]
        public string Reunion { get; set; }

        [NotMapped]
        public short NoDias { get; set; }

        [NotMapped]
        public string NombreUsuario { get; set; }

        [NotMapped]
        public int? IdActa { get; set; }

    }
}

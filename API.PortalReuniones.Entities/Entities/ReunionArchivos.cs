﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class ReunionArchivos
    {
        [Column("Id")]
        public int Id { get; set; }

        [Column("Reunion")]
        public int? Reunion { get; set; }

        [StringLength(255)]
        [Column("nombre")]
        public string Nombre { get; set; }

        [StringLength(255)]
        [Column("ruta")]
        public string Ruta { get; set; }

        [StringLength(255)]
        [Column("usuario")]
        public string Usuario { get; set; }

        [StringLength(255)]
        [Column("tp_Documento")]
        public string TpDocumento { get; set; }
    }
}

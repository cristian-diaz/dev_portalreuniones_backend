﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class Jerarquia
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("Id")]
        public int Id { get; set; }

        [StringLength(255)]
        [Column("Pertenece")]
        public string Pertenece { get; set; }

        [StringLength(255)]
        [Column("Tipo")]
        public string Tipo { get; set; }

        [StringLength(255)]
        [Column("NombreDependencia")]
        public string NombreDependencia { get; set; }
    }
}

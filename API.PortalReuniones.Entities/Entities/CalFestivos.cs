﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class CalFestivos
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("Id")]
        public int Id { get; set; }

        [Column("evendate")]
        public DateTime? Evendate { get; set; }

        [StringLength(255)]
        [Column("EventTitle")]
        public string EventTitle { get; set; }
    }
}

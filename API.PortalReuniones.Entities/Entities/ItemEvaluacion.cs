﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.PortalReuniones.Entities.Entities
{
    public partial class ItemEvaluacion
    {
        [Key]
        [Column("Id_item")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdItem { get; set; }

        [StringLength(255)]
        [Column("Item")]
        public string Item { get; set; }

        [Column("Peso")]
        public int? Peso { get; set; }

        [StringLength(255)]
        [Column("comentario")]
        public string Comentario { get; set; }

        [StringLength(50)]
        [Column("calificacion")]
        public string Calificacion { get; set; }

        [Column("MaxPuntos")]
        public int? MaxPuntos { get; set; }

        [Key]
        [Column("activo")]
        public bool Activo { get; set; }

        [Key]
        [Column("calc")]
        public bool Calc { get; set; }

        [Column("ord")]
        public int? Ord { get; set; }
    }
}

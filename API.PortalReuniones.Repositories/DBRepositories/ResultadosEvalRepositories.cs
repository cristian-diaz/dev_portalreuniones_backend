﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.EntitiesExtension;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using API.PortalReuniones.Repositories.Middle;
using System.Linq;
using System.Threading.Tasks;

namespace API.PortalReuniones.Repositories.DBRepositories
{
    public class ResultadosEvalRepositories : IResultadosEvalRepositories
    {
        private readonly PortalReunionesContext _dbContext;
        public ResultadosEvalRepositories(PortalReunionesContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<PagedResult<ReporteEvaluaciones>> ConsultarReporteEvaluacion(ReporteEvaluacionFiltro data)
        {
            var query = FitroReporteEvaluaciones(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.Nombre,
                           item.TipAsoc,
                           item.Item,
                           item.TipoNm,
                           item.Calificacion,
                           item.Permiso,
                           item.TotalPuntos,
                           item.Login,
                           item.IdItem,
                           item.NombreUsuario,
                           item.Fecha,
                           item.Lugar,
                           item.Comentario,
                           item.HoraInicioReal,
                           item.HoraFinalReal,
                           item.HoraEstimadaInicio,
                           item.HoraEstimadaFinal
                       }
                into res
                       select new ReporteEvaluaciones()
                       {
                           Nombre = res.Key.Nombre,
                           TipAsoc = res.Key.TipAsoc,
                           Item = res.Key.Item,
                           TipoNm = res.Key.TipoNm,
                           Calificacion = res.Key.Calificacion,
                           Permiso = res.Key.Permiso,
                           TotalPuntos = res.Key.TotalPuntos,
                           Login = res.Key.Login,
                           IdItem = res.Key.IdItem,
                           NombreUsuario = res.Key.NombreUsuario,
                           Fecha = res.Key.Fecha,
                           Lugar = res.Key.Lugar,
                           Comentario = res.Key.Comentario,
                           HoraInicioReal = res.Key.HoraInicioReal,
                           HoraFinalReal = res.Key.HoraFinalReal,
                           HoraEstimadaInicio = res.Key.HoraEstimadaInicio,
                           HoraEstimadaFinal = res.Key.HoraEstimadaFinal,
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<ReporteEvaluaciones> FitroReporteEvaluaciones(ReporteEvaluacionFiltro data)
        {
            var query = _dbContext.JoinResultados_EvalReporte(data.TipoNm, data.Permiso).AsQueryable();

            if (!string.IsNullOrWhiteSpace(data.TipoNm))
            {
                query = query.Where(x => x.TipoNm == data.TipoNm);
            }

            if (!string.IsNullOrWhiteSpace(data.Permiso))
            {
                query = query.Where(x => x.Permiso == data.Permiso);
            }

            if (!string.IsNullOrWhiteSpace(data.Nombre))
            {
                query = query.Where(x => x.Nombre == data.Nombre);
            }

            if (data.IdItem > 0)
            {
                query = query.Where(x => x.IdItem == data.IdItem);
            }

            if (data.FechaInicio != null)
            {
                query = query.Where(x => x.Fecha >= data.FechaInicio);
            }

            if (data.FechaFin != null)
            {
                query = query.Where(x => x.Fecha <= data.FechaInicio);
            }

            if (data.HoraInicioReal != null)
            {
                query = query.Where(x => x.HoraInicioReal == data.HoraInicioReal);
            }

            if (data.HoraFinReal != null)
            {
                query = query.Where(x => x.HoraFinalReal == data.HoraFinReal);
            }

            if (data.TotalPuntos > 0)
            {
                query = query.Where(x => x.TotalPuntos >= data.TotalPuntos);
            }

            return query;
        }
    }
}

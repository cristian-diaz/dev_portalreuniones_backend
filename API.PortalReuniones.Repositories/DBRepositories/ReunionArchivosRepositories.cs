﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using API.PortalReuniones.Repositories.Middle;
using System.Linq;
using System.Threading.Tasks;

namespace API.PortalReuniones.Repositories.DBRepositories
{
    public class ReunionArchivosRepositories : IReunionArchivosRepositories
    {
        private readonly PortalReunionesContext _dbContext;
        public ReunionArchivosRepositories(PortalReunionesContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<PagedResult<ReunionArchivos>> ConsultarReunionArchivos(ReunionArchivosFiltro data)
        { 
            var query = Filtro(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.Id,
                           item.Reunion,
                           item.Nombre,
                           item.Ruta,
                           item.Usuario,
                           item.TpDocumento,
                       }
                into res
                       select new ReunionArchivos()
                       {
                           Id = res.Key.Id,
                           Reunion = res.Key.Reunion,
                           Nombre = res.Key.Nombre,
                           Ruta = res.Key.Ruta,
                           Usuario = res.Key.Usuario,
                           TpDocumento = res.Key.TpDocumento,
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<ReunionArchivos> Filtro(ReunionArchivosFiltro data)
        {
            var query = _dbContext.Reunion_Archivos().AsQueryable();

            if (data.Id > 0)
            {
                query = query.Where(x => x.Id == data.Id);
            }

            if (data.Reunion > 0)
            {
                query = query.Where(x => x.Reunion == data.Reunion);
            }

            if (!string.IsNullOrWhiteSpace(data.Nombre))
            {
                query = query.Where(x => x.Nombre == data.Nombre);
            }

            if (!string.IsNullOrWhiteSpace(data.Ruta))
            {
                query = query.Where(x => x.Ruta == data.Ruta);
            }

            if (!string.IsNullOrWhiteSpace(data.Usuario))
            {
                query = query.Where(x => x.Usuario == data.Usuario);
            }

            if (!string.IsNullOrWhiteSpace(data.TpDocumento))
            {
                query = query.Where(x => x.TpDocumento == data.TpDocumento);
            }

            return query;
        }
    }
}

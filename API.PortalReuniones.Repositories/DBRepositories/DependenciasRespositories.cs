using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Paged;
using API.PortalReuniones.Repositories.Middle;
using System.Threading.Tasks;
using System.Linq;
using API.PortalReuniones.Entities.Filtro;

namespace API.PortalReuniones.Repositories.DBRepositories
{
    public class DependenciasRespositories: IDependenciaRepositories
    {
        private readonly PortalReunionesContext _dbContext;
        public DependenciasRespositories(PortalReunionesContext dbContext){
            _dbContext = dbContext;
        }
        public async Task<PagedResult<Dependencia>> GetDepencias(){
            var query = _dbContext.DEPENDENCIA;
            var sql = await query.Paginate(0, 0);
            return sql;
        }

        public async Task<PagedResult<Dependencia>> ConsultarDEPENDENCIA(DEPENDENCIAFiltro data)
        {
            var query = Filter(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.DependenciaId,
                           item.TipoDepId,
                           item.DepPadreId,
                           item.DependenciaNombre,
                       }
                into res
                       select new Dependencia()
                       {
                           DependenciaId = res.Key.DependenciaId,
                           TipoDepId = res.Key.TipoDepId,
                           DepPadreId = res.Key.DepPadreId,
                           DependenciaNombre = res.Key.DependenciaNombre,
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<Dependencia> Filter(DEPENDENCIAFiltro data)
        {
            var query = _dbContext.DEPENDENCIA().AsQueryable();

            if (!string.IsNullOrWhiteSpace(data.DependenciaId))
            {
                query = query.Where(x => x.DependenciaId == data.DependenciaId);
            }

            if (data.TipoDepId > 0)
            {
                query = query.Where(x => x.TipoDepId == data.TipoDepId);
            }

            if (!string.IsNullOrWhiteSpace(data.DepPadreId))
            {
                query = query.Where(x => x.DepPadreId == data.DepPadreId);
            }

            if (!string.IsNullOrWhiteSpace(data.DependenciaNombre))
            {
                query = query.Where(x => x.DependenciaNombre == data.DependenciaNombre);
            }

            return query;
        }
    }
}
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.EntitiesExtension;
using System.Linq;

namespace API.PortalReuniones.Repositories.DBRepositories
{
    public static class JoinRepository
    {
        public static IQueryable<Compromisos> JoinCompromisos(this PortalReunionesContext context, int State, bool isManager = false)
        {
            var query = from wh in context.Compromisos
                        join tLogins in context.t_logins on wh.Login equals tLogins.Login
                        join delegado in context.t_logins on wh.Delegado equals delegado.Login
                        //join per in context.Periodicidad on wh.Id_Periodicidad equals per.Id_Periodicidad    
                        select new Compromisos()
                        {
                            NoId = wh.NoId,
                            Tipo = wh.Tipo,
                            Nombre = wh.Nombre,
                            Tema = wh.Tema,
                            Accion = wh.Accion,
                            Solucion = wh.Solucion,
                            ArchivoAsociado = wh.ArchivoAsociado,
                            Responsable = wh.Responsable,
                            FechaI = wh.FechaI,
                            FechaC = wh.FechaC,
                            Status = wh.Status,
                            Email = tLogins.EMail,
                            FechaReunion = wh.FechaReunion,
                            HoraReunion = wh.HoraReunion,
                            NombreUsuario = tLogins.NombreUsuario
                        };

            return query.AsQueryable();
        }

        public static IQueryable<Compromisos> JoinCompromisosFiltros(this PortalReunionesContext context, int state, bool isManager = false)
        {
            var query = from wh in context.Compromisos
                        join tLogins in context.t_logins on wh.Login equals tLogins.Login
                        join delegado in context.t_logins on wh.Delegado equals delegado.Login
                        join reunion in context.TipoReunion on wh.Tipo equals reunion.Nombre
                        join per in context.Periodicidad on wh.IdPeriodicidad equals per.IdPeriodicidad
                        select new Compromisos()
                        {
                            NoId = wh.NoId,
                            Tipo = wh.Tipo,
                            Nombre = wh.Nombre,
                            Accion = wh.Accion,
                            Solucion = wh.Solucion,
                            ArchivoAsociado = wh.ArchivoAsociado,
                            Responsable = wh.Responsable,
                            FechaI = wh.FechaI,
                            FechaC = wh.FechaC,
                            Status = wh.Status,
                            Reunion = reunion.Nombre,
                            Tema = wh.Tema,
                            Delegado = wh.Delegado,
                            IdPeriodicidad = per.IdPeriodicidad,
                            NoDias = per.NoDias,
                            NombreUsuario = tLogins.NombreUsuario,
                            Login = wh.Login
                        };

            return query.AsQueryable();
        }

        public static IQueryable<Compromisos> JoinCompromisosActaFiltros(this PortalReunionesContext context, int state)
        {
            var query = from wh in context.Compromisos
                        join ac in context.Acta_Compromisos on wh.NoId equals ac.IdCompromisos
                        select new Compromisos()
                        {
                            NoId = wh.NoId,
                            Tipo = wh.Tipo,
                            Nombre = wh.Nombre,
                            Accion = wh.Accion,
                            Solucion = wh.Solucion,
                            ArchivoAsociado = wh.ArchivoAsociado,
                            Responsable = wh.Responsable,
                            FechaI = wh.FechaI,
                            FechaC = wh.FechaC,
                            Status = wh.Status,
                            Tema = wh.Tema,
                            Delegado = wh.Delegado,
                            State = wh.State,
                            IdActa = ac.IdActa
                        };
            //query = query.Where(x => x.state == state);
            return query.AsQueryable();
        }

        public static IQueryable<TipoReunion> TipoReunion(this PortalReunionesContext context, int state = -1)
        {
            var query = from tr in context.TipoReunion select tr;
            query = query.Where(x => x.Activa == state);
            return query.AsQueryable();
        }

        public static IQueryable<TipoReunion> JoinTipoReunion(this PortalReunionesContext context, int state, bool isManager = false)
        {
            var query = from tr in context.TipoReunion
                        join perm in context.permisos on tr.Id equals perm.IdReunion
                        select new TipoReunion()
                        {
                            Id = tr.Id,
                            Nombre = tr.Nombre,
                            FechaInicio = tr.FechaInicio,
                            FechaFinaliza = tr.FechaFinaliza,
                            FechaIngreso = tr.FechaIngreso,
                            Area = tr.Area,
                            Moderador = tr.Moderador,
                            IdRecurso = tr.IdRecurso,
                            TipAsoc = tr.TipAsoc,
                            TipoNm = tr.TipoNm,
                            Activa = tr.Activa,
                            HoraInicio = tr.HoraInicio,
                            HoraFin = tr.HoraFin,
                            Duracion = tr.Duracion,
                            ValidarAsistentes = tr.ValidarAsistentes,
                            Seguimiento = tr.Seguimiento,
                            Login = perm.Login,
                            Permiso = perm.Permiso
                        };
            return query.AsQueryable();
        }

        public static IQueryable<TLogins> t_logins(this PortalReunionesContext context, bool state = true)
        {
            var query = from tr in context.t_logins select tr;
            query = query.Where(x => x.Activo == state);
            return query.AsQueryable();
        }

        public static IQueryable<TLogins> JoinT_loginsPermisos(this PortalReunionesContext context)
        {
            var query = from tl in context.t_logins
                        join perm in context.permisos on tl.Login equals perm.Login
                        select new TLogins()
                        {
                            Login = tl.Login,
                            Grupo = tl.Grupo,
                            NombreUsuario = tl.NombreUsuario,
                            Rol = tl.Rol,
                            Administrador = tl.Administrador,
                            EMail = tl.EMail,
                            Activo = tl.Activo,
                            IdReunion = perm.IdReunion,
                        };
            return query.AsQueryable();
        }

        public static IQueryable<Acta> Acta(this PortalReunionesContext context, int state = -1)
        {
            var query = from ac in context.Acta select ac;
            return query.AsQueryable();
        }

        public static IQueryable<Dependencia> DEPENDENCIA(this PortalReunionesContext context, int state = -1)
        {
            var query = from d in context.DEPENDENCIA select d;
            return query.AsQueryable();
        }

        public static IQueryable<ReporteCompromisos> JoinCompromisosReporte(this PortalReunionesContext context)
        {
            var query = from wh in context.Compromisos
                        join delegado in context.t_logins on wh.Delegado equals delegado.Login
                        select new ReporteCompromisos()
                        {
                            Tipo = wh.Tipo,
                            Tema = wh.Tema,
                            Accion = wh.Accion,
                            Responsable = wh.Responsable,
                            Solucion = wh.Solucion,
                            FechaC = wh.FechaC,
                            Status = wh.Status,
                            Verificado = wh.Verficado,
                            NombreUsuario = delegado.NombreUsuario
                        };

            return query.AsQueryable();
        }

        public static IQueryable<ReporteEvaluaciones> JoinResultados_EvalReporte(this PortalReunionesContext context, string Tipo, string Permiso)
        {
            var query = from tr in context.TipoReunion
                        join a in context.AsistentesReunion on tr.Id equals a.IdReunion
                        join p in context.permisos on tr.Id equals p.IdReunion
                        join re in context.Resultados_Eval on a.NoEval equals re.NoEval
                        join rc in context.Resultados_Calif on a.NoEval equals rc.NoEval
                        join i in context.Item_Evaluacion on rc.IdItem equals i.IdItem
                        join tl in context.t_logins on p.Login equals tl.Login
                        where tr.TipoNm == Tipo && tr.Permiso == Permiso
                        select new ReporteEvaluaciones()
                        {
                            Nombre = tr.Nombre,
                            TipAsoc = tr.TipAsoc,
                            TipoNm = tr.TipoNm,
                            IdItem = i.IdItem,
                            Item = i.Item,
                            Calificacion = rc.Calificacion,
                            Permiso = p.Permiso,
                            TotalPuntos = re.TotalPuntos,
                            Login = tl.Login,
                            NombreUsuario = tl.NombreUsuario,
                            Fecha = re.Fecha,
                            Lugar = re.Lugar,
                            Comentario = re.Comentario,
                            HoraInicioReal = re.HoraInicioReal,
                            HoraFinalReal = re.HoraFinalReal,
                            HoraEstimadaInicio = re.HoraEstimadaInicio,
                            HoraEstimadaFinal = re.HoraEstimadaFinal
                        };
            return query.AsQueryable();
        }

        public static IQueryable<ReunionArchivos> Reunion_Archivos(this PortalReunionesContext context)
        {
            var query = from ra in context.Reunion_Archivos select ra;
            return query.AsQueryable();
        }

        public static IQueryable<TipoReunion> JoinTipoReunionIndicador(this PortalReunionesContext context)
        {
            var query = from tr in context.TipoReunion
                        join j in context.Jerarquia on tr.Area equals j.Id
                        select new TipoReunion()
                        {
                            Id = tr.Id,
                            Nombre = tr.Nombre,
                            Tipo = j.Tipo,
                        };
            return query.AsQueryable();
        }

        public static IQueryable<ItemEvaluacion> Item_Evaluacion(this PortalReunionesContext context)
        {
            var query = from ie in context.Item_Evaluacion select ie;
            return query.AsQueryable();
        }

        public static IQueryable<ControlCambios> ControlCambios(this PortalReunionesContext context)
        {
            var query = from cc in context.ControlCambios select cc;
            return query.AsQueryable();
        }
    }
}
﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using API.PortalReuniones.Repositories.Middle;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace API.PortalReuniones.Repositories.DBRepositories
{
    public class ControlCambiosRepositories : IControlCambiosRepositories
    {
        private readonly PortalReunionesContext _dbContext;
        public ControlCambiosRepositories(PortalReunionesContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PagedResult<ControlCambios>> ConsultarControlCambios(ControlCambiosFiltro data)
        {
            var query = Filtro(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.NoId,
                           item.CompromisoId,
                           item.Tipo,
                           item.Tema,
                           item.Accion,
                           item.Responsable,
                           item.FechaI,
                           item.FechaC,
                           item.Status,
                           item.Login,
                           item.Solucion,
                           item.ArchivoAsociado,
                           item.Verificado,
                           item.FechaRealCierre,
                           item.FechaCambio,
                           item.MotivoCambio,
                           item.NuevoResponsable,
                           item.ModificadoPor,
                           item.TipoDeCambio,
                           item.HoraCierre,
                           item.NombreNuevoResp,
                           item.Priorizacion,
                           item.Categoria,
                       }
                into res
                       select new ControlCambios()
                       {
                           NoId = res.Key.NoId,
                           CompromisoId = res.Key.CompromisoId,
                           Tipo = res.Key.Tipo,
                           Tema = res.Key.Tema,
                           Accion = res.Key.Accion,
                           Responsable = res.Key.Responsable,
                           FechaI = res.Key.FechaI,
                           FechaC = res.Key.FechaC,
                           Status = res.Key.Status,
                           Login = res.Key.Login,
                           Solucion = res.Key.Solucion,
                           ArchivoAsociado = res.Key.ArchivoAsociado,
                           Verificado = res.Key.Verificado,
                           FechaRealCierre = res.Key.FechaRealCierre,
                           FechaCambio = res.Key.FechaCambio,
                           MotivoCambio = res.Key.MotivoCambio,
                           NuevoResponsable = res.Key.NuevoResponsable,
                           ModificadoPor = res.Key.ModificadoPor,
                           TipoDeCambio = res.Key.TipoDeCambio,
                           HoraCierre = res.Key.HoraCierre,
                           NombreNuevoResp = res.Key.NombreNuevoResp,
                           Priorizacion = res.Key.Priorizacion,
                           Categoria = res.Key.Categoria,
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<ControlCambios> Filtro(ControlCambiosFiltro data)
        {
            var query = _dbContext.ControlCambios().AsQueryable();

            if (data.NoId > 0)
            {
                query = query.Where(x => x.NoId == data.NoId);
            }

            if (data.CompromisoId > 0)
            {
                query = query.Where(x => x.CompromisoId == data.CompromisoId);
            }

            if (!string.IsNullOrWhiteSpace(data.Tipo))
            {
                query = query.Where(x => x.Tipo == data.Tipo);
            }

            if (!string.IsNullOrWhiteSpace(data.Tema))
            {
                query = query.Where(x => x.Tema == data.Tema);
            }

            if (!string.IsNullOrWhiteSpace(data.Accion))
            {
                query = query.Where(x => x.Accion == data.Accion);
            }

            if (!string.IsNullOrWhiteSpace(data.Responsable))
            {
                query = query.Where(x => x.Responsable == data.Responsable);
            }

            if (data.FechaI != null)
            {
                query = query.Where(x => x.FechaI == data.FechaI);
            }

            if (data.FechaC != null)
            {
                query = query.Where(x => x.FechaC == data.FechaC);
            }

            if (!string.IsNullOrWhiteSpace(data.Status))
            {
                query = query.Where(x => x.Status == data.Status);
            }

            if (!string.IsNullOrWhiteSpace(data.Login))
            {
                query = query.Where(x => x.Login == data.Login);
            }

            if (!string.IsNullOrWhiteSpace(data.Solucion))
            {
                query = query.Where(x => x.Solucion == data.Solucion);
            }

            if (!string.IsNullOrWhiteSpace(data.ArchivoAsociado))
            {
                query = query.Where(x => x.ArchivoAsociado == data.ArchivoAsociado);
            }

            if (data.Verificado != null)
            {
                query = query.Where(x => x.Verificado == data.Verificado);
            }

            if (data.FechaRealCierre != null)
            {
                query = query.Where(x => x.FechaRealCierre == data.FechaRealCierre);
            }

            if (data.FechaCambio != null)
            {
                query = query.Where(x => x.FechaCambio == data.FechaCambio);
            }

            if (!string.IsNullOrWhiteSpace(data.MotivoCambio))
            {
                query = query.Where(x => x.MotivoCambio == data.MotivoCambio);
            }

            if (!string.IsNullOrWhiteSpace(data.NuevoResponsable))
            {
                query = query.Where(x => x.NuevoResponsable == data.NuevoResponsable);
            }

            if (!string.IsNullOrWhiteSpace(data.ModificadoPor))
            {
                query = query.Where(x => x.ModificadoPor == data.ModificadoPor);
            }

            if (data.TipoDeCambio > 0)
            {
                query = query.Where(x => x.TipoDeCambio == data.TipoDeCambio);
            }

            if (data.HoraCierre != null)
            {
                query = query.Where(x => x.HoraCierre == data.HoraCierre);
            }

            if (!string.IsNullOrWhiteSpace(data.NombreNuevoResp))
            {
                query = query.Where(x => x.NombreNuevoResp == data.NombreNuevoResp);
            }

            if (!string.IsNullOrWhiteSpace(data.Priorizacion))
            {
                query = query.Where(x => x.Priorizacion == data.Priorizacion);
            }

            if (!string.IsNullOrWhiteSpace(data.Categoria))
            {
                query = query.Where(x => x.Categoria == data.Categoria);
            }

            return query;
        }

        public async Task<int> ActualizarControlCambios(ControlCambios viewModel)
        {
            var dato = _dbContext.ControlCambios.Where(m => m.NoId == viewModel.NoId).FirstOrDefault();
            if (dato.CompromisoId != viewModel.CompromisoId)
                dato.CompromisoId = viewModel.CompromisoId;
            if (viewModel.Tipo != null)
                dato.Tipo = viewModel.Tipo;
            if (viewModel.Tema != null)
                dato.Tema = viewModel.Tema;
            if (viewModel.Accion != null)
                dato.Accion = viewModel.Accion;
            if (viewModel.Responsable != null)
                dato.Responsable = viewModel.Responsable;
            if (viewModel.FechaI != null)
                dato.FechaI = viewModel.FechaI;
            if (viewModel.FechaC != null)
                dato.FechaC = viewModel.FechaC;
            if (viewModel.Status != null)
                dato.Status = viewModel.Status;
            if (viewModel.Login != null)
                dato.Login = viewModel.Login;
            if (viewModel.Solucion != null)
                dato.Solucion = viewModel.Solucion;
            if (viewModel.ArchivoAsociado != null)
                dato.ArchivoAsociado = viewModel.ArchivoAsociado;
            if (dato.Verificado != viewModel.Verificado)
                dato.Verificado = viewModel.Verificado;
            if (viewModel.FechaRealCierre != null)
                dato.FechaRealCierre = viewModel.FechaRealCierre;
            if (viewModel.FechaCambio != null)
                dato.FechaCambio = viewModel.FechaCambio;
            if (viewModel.MotivoCambio != null)
                dato.MotivoCambio = viewModel.MotivoCambio;
            if (viewModel.NuevoResponsable != null)
                dato.NuevoResponsable = viewModel.NuevoResponsable;
            if (viewModel.ModificadoPor != null)
                dato.ModificadoPor = viewModel.ModificadoPor;
            if (viewModel.TipoDeCambio != null)
                dato.TipoDeCambio = viewModel.TipoDeCambio;
            if (viewModel.HoraCierre != null)
                dato.HoraCierre = viewModel.HoraCierre;
            if (viewModel.NombreNuevoResp != null)
                dato.NombreNuevoResp = viewModel.NombreNuevoResp;
            if (viewModel.Priorizacion != null)
                dato.Priorizacion = viewModel.Priorizacion;
            if (viewModel.Categoria != null)
                dato.Categoria = viewModel.Categoria;

            _dbContext.Entry(dato).State = EntityState.Modified;
            return await _dbContext.SaveChangesAsync();
        }
    }
}

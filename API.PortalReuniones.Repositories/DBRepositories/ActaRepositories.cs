﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using API.PortalReuniones.Repositories.Middle;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace API.PortalReuniones.Repositories.DBRepositories
{
    public class ActaRepositories : IActaRepositories
    {
        private readonly PortalReunionesContext _dbContext;
        public ActaRepositories(PortalReunionesContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> GuardarActa(Acta data)
        { 
            _dbContext.Acta.Add(data);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<PagedResult<Acta>> ConsultarActa(ActaFiltro data)
        {
            var query = Filter(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.IdActa,
                           item.NumActa,
                           item.FechaActa,
                           item.Estado,
                           item.CodigoFormato,
                           item.Version,
                           item.NombreActa,
                           item.DepartamentoActa,
                           item.TemaActa,
                           item.Ubicacion,
                           item.FechaEval,
                           item.HoraInicio,
                           item.HoraFin,
                           item.Objetivo,
                           item.EvalReunion,
                           item.Asistentes,
                           item.Reviso,
                           item.Aprobo,
                           item.CorreoAprobo,
                       }
                into res
                       select new Acta()
                       {
                           IdActa = res.Key.IdActa,
                           NumActa = res.Key.NumActa,
                           FechaActa = res.Key.FechaActa,
                           Estado = res.Key.Estado,
                           CodigoFormato = res.Key.CodigoFormato,
                           Version = res.Key.Version,
                           NombreActa = res.Key.NombreActa,
                           DepartamentoActa = res.Key.DepartamentoActa,
                           TemaActa = res.Key.TemaActa,
                           Ubicacion = res.Key.Ubicacion,
                           FechaEval = res.Key.FechaEval,
                           HoraInicio = res.Key.HoraInicio,
                           HoraFin = res.Key.HoraFin,
                           Objetivo = res.Key.Objetivo,
                           EvalReunion = res.Key.EvalReunion,
                           Asistentes = res.Key.Asistentes,
                           Reviso = res.Key.Reviso,
                           Aprobo = res.Key.Aprobo,
                           CorreoAprobo = res.Key.CorreoAprobo
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<Acta> Filter(ActaFiltro data)
        {
            var query = _dbContext.Acta().AsQueryable();

            if (data.IdActa > 0)
            {
                query = query.Where(x => x.IdActa == data.IdActa);
            }

            if (data.NumActa > 0)
            {
                query = query.Where(x => x.NumActa == data.NumActa);
            }

            if (data.FechaActa != null)
            {
                query = query.Where(x => x.FechaActa == data.FechaActa);
            }

            if (data.Estado > 0)
            {
                query = query.Where(x => x.Estado == data.Estado);
            }

            if (!string.IsNullOrWhiteSpace(data.NombreActa))
            {
                query = query.Where(x => x.NombreActa == data.NombreActa);
            }

            if (!string.IsNullOrWhiteSpace(data.TemaActa))
            {
                query = query.Where(x => x.TemaActa == data.TemaActa);
            }

            if (data.FechaEval != null)
            {
                query = query.Where(x => x.FechaEval == data.FechaEval);
            }

            if (data.HoraInicio != null)
            {
                query = query.Where(x => x.HoraInicio == data.HoraInicio);
            }

            if (data.HoraFin != null)
            {
                query = query.Where(x => x.HoraFin == data.HoraFin);
            }

            return query;
        }

        public async Task<int> ActualizarActa(Acta viewModel)
        {
            var dato = _dbContext.Acta.Where(m => m.IdActa == viewModel.IdActa).FirstOrDefault();
            if (viewModel.NumActa > 0)
                dato.NumActa = viewModel.NumActa;
            if (viewModel.FechaActa != null)
                dato.FechaActa = viewModel.FechaActa;
            if (viewModel.Estado > 0)
                dato.Estado = viewModel.Estado;
            if (viewModel.CodigoFormato != null)
                dato.CodigoFormato = viewModel.CodigoFormato;
            if (viewModel.Version != null)
                dato.Version = viewModel.Version;
            if (viewModel.NombreActa != null)
                dato.NombreActa = viewModel.NombreActa;
            if (viewModel.DepartamentoActa != null)
                dato.DepartamentoActa = viewModel.DepartamentoActa;
            if (viewModel.TemaActa != null)
                dato.TemaActa = viewModel.TemaActa;
            if (viewModel.Ubicacion != null)
                dato.Ubicacion = viewModel.Ubicacion;
            if (viewModel.FechaEval != null)
                dato.FechaEval = viewModel.FechaEval;
            if (viewModel.HoraInicio != null)
                dato.HoraInicio = viewModel.HoraInicio;
            if (dato.HoraFin != null)
                dato.HoraFin = viewModel.HoraFin;
            if (viewModel.Objetivo != null)
                dato.Objetivo = viewModel.Objetivo;
            if (viewModel.EvalReunion > 0)
                dato.EvalReunion = viewModel.EvalReunion;
            if (viewModel.Asistentes != null)
                dato.Asistentes = viewModel.Asistentes;
            if (viewModel.Reviso != null)
                dato.Reviso = viewModel.Reviso;
            if (viewModel.Aprobo != null)
                dato.Aprobo = viewModel.Aprobo;
            if (viewModel.CorreoAprobo != null)
                dato.CorreoAprobo = viewModel.CorreoAprobo;

            _dbContext.Entry(dato).State = EntityState.Modified;
            return await _dbContext.SaveChangesAsync();
        }
    }
}

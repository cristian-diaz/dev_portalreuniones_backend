﻿
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using API.PortalReuniones.Repositories.Middle;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace API.PortalReuniones.Repositories.DBRepositories
{
    public class TipoReunionRepositories : ITipoReunionRepositories
    {
        private readonly PortalReunionesContext _dbContext;
        public TipoReunionRepositories(PortalReunionesContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PagedResult<TipoReunion>> ConsultarTipoReunion(TipoReunionFiltro data)
        {
            var query = Filter(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.Id,
                           item.Nombre,
                           item.FechaInicio,
                           item.FechaFinaliza,
                           item.FechaIngreso,
                           item.Area,
                           item.Moderador,
                           item.IdRecurso,
                           item.TipAsoc,
                           item.TipoNm,
                           item.Activa,
                           item.HoraInicio,
                           item.HoraFin,
                           item.Duracion,
                           item.ValidarAsistentes,
                           item.Seguimiento
                       }
                into res
                       select new TipoReunion()
                       {
                           Id = res.Key.Id,
                           Nombre = res.Key.Nombre,
                           FechaInicio = res.Key.FechaInicio,
                           FechaFinaliza = res.Key.FechaFinaliza,
                           FechaIngreso = res.Key.FechaIngreso,
                           Area = res.Key.Area,
                           Moderador = res.Key.Moderador,
                           IdRecurso = res.Key.IdRecurso,
                           TipAsoc = res.Key.TipAsoc,
                           TipoNm = res.Key.TipoNm,
                           Activa = res.Key.Activa,
                           HoraInicio = res.Key.HoraInicio,
                           HoraFin = res.Key.HoraFin,
                           Duracion = res.Key.Duracion,
                           ValidarAsistentes = res.Key.ValidarAsistentes,
                           Seguimiento = res.Key.Seguimiento
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<TipoReunion> Filter(TipoReunionFiltro data)
        {
            var query = _dbContext.TipoReunion(-1).AsQueryable();

            if (data.Id > 0)
            {
                query = query.Where(x => x.Id == data.Id);
            }

            if (!string.IsNullOrWhiteSpace(data.Nombre))
            {
                query = query.Where(x => x.Nombre == data.Nombre);
            }

            if (data.Activa != null)
            {
                query = query.Where(x => x.Activa == data.Activa);
            }

            if (!string.IsNullOrWhiteSpace(data.TipoNm))
            {
                query = query.Where(x => x.TipoNm == data.TipoNm);
            }

            if (data.TipAsoc != null)
            {
                query = query.Where(x => x.TipAsoc == data.TipAsoc);
            }

            if (data.HoraInicio != null)
            {
                query = query.Where(x => x.HoraInicio == data.HoraInicio);
            }

            if (data.HoraFin != null)
            {
                query = query.Where(x => x.HoraFin == data.HoraFin);
            }

            return query;
        }

        private IQueryable<TipoReunion> FilterJoin(PresentarTipoReunionFiltro data)
        {
            var query = _dbContext.JoinTipoReunion(-1).AsQueryable();

            if (!string.IsNullOrWhiteSpace(data.Login))
            {
                query = query.Where(x => x.Login == data.Login);
            }

            if (!string.IsNullOrWhiteSpace(data.TipoNm))
            {
                query = query.Where(x => x.TipoNm == data.TipoNm);
            }

            if (!string.IsNullOrWhiteSpace(data.Permiso))
            {
                query = query.Where(x => x.Permiso == data.Permiso);
            }

            return query;
        }

        public async Task<int> GuardarTipoReunion(TipoReunion data)
        {
            _dbContext.TipoReunion.Add(data);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<int> ActualizarTipoReunion(TipoReunion viewModel)
        {
            var dato = _dbContext.TipoReunion.Where(m => m.Id == viewModel.Id).FirstOrDefault();
            if (viewModel.Nombre != null)
                dato.Nombre = viewModel.Nombre;
            if (viewModel.FechaInicio != null)
                dato.FechaInicio = viewModel.FechaInicio;
            if (viewModel.FechaFinaliza != null)
                dato.FechaFinaliza = viewModel.FechaFinaliza;
            if (viewModel.FechaIngreso != null)
                dato.FechaIngreso = viewModel.FechaIngreso;
            if (viewModel.Area != null)
                dato.Area = viewModel.Area;
            if (viewModel.Moderador != null)
                dato.Moderador = viewModel.Moderador;
            if (viewModel.IdRecurso != null)
                dato.IdRecurso = viewModel.IdRecurso;
            if (viewModel.TipAsoc != null)
                dato.TipAsoc = viewModel.TipAsoc;
            if (viewModel.TipoNm != null)
                dato.TipoNm = viewModel.TipoNm;
            if (dato.Activa != viewModel.Activa)
                dato.Activa = viewModel.Activa;
            if (viewModel.HoraInicio != null)
                dato.HoraInicio = viewModel.HoraInicio;
            if (dato.HoraFin != null)
                dato.HoraFin = viewModel.HoraFin;
            if (viewModel.Duracion != null)
                dato.Duracion = viewModel.Duracion;
            if (dato.ValidarAsistentes != viewModel.ValidarAsistentes)
                dato.ValidarAsistentes = viewModel.ValidarAsistentes;
            if (viewModel.Seguimiento != null)
                dato.Seguimiento = viewModel.Seguimiento;

            _dbContext.Entry(dato).State = EntityState.Modified;
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<PagedResult<TipoReunion>> ConsultarTipoReunionFiltro(PresentarTipoReunionFiltro data)
        {
            var query = FilterJoin(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.Id,
                           item.Nombre,
                           item.FechaInicio,
                           item.FechaFinaliza,
                           item.FechaIngreso,
                           item.Area,
                           item.Moderador,
                           item.IdRecurso,
                           item.TipAsoc,
                           item.TipoNm,
                           item.Activa,
                           item.HoraInicio,
                           item.HoraFin,
                           item.Duracion,
                           item.ValidarAsistentes,
                           item.Seguimiento,
                           item.Login,
                           item.Permiso
                       }
                into res
                       select new TipoReunion()
                       {
                           Id = res.Key.Id,
                           Nombre = res.Key.Nombre,
                           FechaInicio = res.Key.FechaInicio,
                           FechaFinaliza = res.Key.FechaFinaliza,
                           FechaIngreso = res.Key.FechaIngreso,
                           Area = res.Key.Area,
                           Moderador = res.Key.Moderador,
                           IdRecurso = res.Key.IdRecurso,
                           TipAsoc = res.Key.TipAsoc,
                           TipoNm = res.Key.TipoNm,
                           Activa = res.Key.Activa,
                           HoraInicio = res.Key.HoraInicio,
                           HoraFin = res.Key.HoraFin,
                           Duracion = res.Key.Duracion,
                           ValidarAsistentes = res.Key.ValidarAsistentes,
                           Seguimiento = res.Key.Seguimiento,
                           Login = res.Key.Login,
                           Permiso = res.Key.Permiso
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        public async Task<PagedResult<TipoReunion>> ConsultarTipoReunionIndicador(TipoReunionIndicadorFiltro data)
        {
            var query = FiltroIndicador(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.Id,
                           item.Nombre,
                           item.Tipo,
                       }
                into res
                       select new TipoReunion()
                       {
                           Id = res.Key.Id,
                           Nombre = res.Key.Nombre,
                           Tipo = res.Key.Tipo,
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<TipoReunion> FiltroIndicador(TipoReunionIndicadorFiltro data)
        {
            var query = _dbContext.JoinTipoReunionIndicador().AsQueryable();

            if (!string.IsNullOrWhiteSpace(data.tipo))
            {
                query = query.Where(x => x.Tipo == data.tipo);
            }

            return query;
        }
    }
}

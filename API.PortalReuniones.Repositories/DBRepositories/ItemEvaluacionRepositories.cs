﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using API.PortalReuniones.Repositories.Middle;
using System.Linq;
using System.Threading.Tasks;

namespace API.PortalReuniones.Repositories.DBRepositories
{
    public class ItemEvaluacionRepositories : IItemEvaluacionRepositories
    {
        private readonly PortalReunionesContext _dbContext;
        public ItemEvaluacionRepositories(PortalReunionesContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PagedResult<ItemEvaluacion>> ConsultarItemEvaluacion(ItemEvaluacionFiltro data)
        {
            var query = Filtro(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.IdItem,
                           item.Item,
                           item.Peso,
                           item.Comentario,
                           item.Calificacion,
                           item.MaxPuntos,
                           item.Activo,
                           item.Calc,
                           item.Ord,
                       }
                into res
                       select new ItemEvaluacion()
                       {
                           IdItem = res.Key.IdItem,
                           Item = res.Key.Item,
                           Peso = res.Key.Peso,
                           Comentario = res.Key.Comentario,
                           Calificacion = res.Key.Calificacion,
                           MaxPuntos = res.Key.MaxPuntos,
                           Activo = res.Key.Activo,
                           Calc = res.Key.Calc,
                           Ord = res.Key.Ord,
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<ItemEvaluacion> Filtro(ItemEvaluacionFiltro data)
        {
            var query = _dbContext.Item_Evaluacion().AsQueryable();

            if (data.IdItem > 0)
            {
                query = query.Where(x => x.IdItem == data.IdItem);
            }

            if (!string.IsNullOrWhiteSpace(data.Item))
            {
                query = query.Where(x => x.Item == data.Item);
            }

            if (data.Peso > 0)
            {
                query = query.Where(x => x.Peso == data.Peso);
            }

            if (!string.IsNullOrWhiteSpace(data.Comentario))
            {
                query = query.Where(x => x.Comentario == data.Comentario);
            }

            if (!string.IsNullOrWhiteSpace(data.Calificacion))
            {
                query = query.Where(x => x.Calificacion == data.Calificacion);
            }

            if (data.MaxPuntos > 0)
            {
                query = query.Where(x => x.MaxPuntos == data.MaxPuntos);
            }

            if (data.Activo != null)
            {
                query = query.Where(x => x.Activo == data.Activo);
            }

            if (data.Calc != null)
            {
                query = query.Where(x => x.Calc == data.Calc);
            }

            if (data.Ord > 0)
            {
                query = query.Where(x => x.Ord == data.Ord);
            }

            return query;
        }
    }
}

﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Paged;
using API.PortalReuniones.Repositories.Middle;
using System.Linq;
using System.Threading.Tasks;

namespace API.PortalReuniones.Repositories.DBRepositories
{
    public class T_loginsRepositories : IT_loginsRepositories
    {
        private readonly PortalReunionesContext _dbContext;
        public T_loginsRepositories(PortalReunionesContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PagedResult<TLogins>> ConsultarT_logins(TLoginsFiltro data)
        {
            var query = Filter(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.Login,
                           item.Grupo,
                           item.NombreUsuario,
                           item.Rol,
                           item.Administrador,
                           item.EMail,
                           item.Activo,
                       }
                into res
                       select new TLogins()
                       {
                           Login = res.Key.Login,
                           Grupo = res.Key.Grupo,
                           NombreUsuario = res.Key.NombreUsuario,
                           Rol = res.Key.Rol,
                           Administrador = res.Key.Administrador,
                           EMail = res.Key.EMail,
                           Activo = res.Key.Activo,
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<TLogins> Filter(TLoginsFiltro data)
        {
            var query = _dbContext.t_logins((bool)data.Activo).AsQueryable();

            if (!string.IsNullOrWhiteSpace(data.Login))
            {
                query = query.Where(x => x.Login == data.Login);
            }

            if (!string.IsNullOrWhiteSpace(data.NombreUsuario))
            {
                query = query.Where(x => x.NombreUsuario.Contains(data.NombreUsuario));
            }

            if (data.Rol > 0)
            {
                query = query.Where(x => x.Rol == data.Rol);
            }

            if (data.Administrador > 0)
            {
                query = query.Where(x => x.Administrador == data.Administrador);
            }

            if (data.EMail != null)
            {
                query = query.Where(x => x.EMail == data.EMail);
            }

            if (data.Grupo != null)
            {
                query = query.Where(x => x.Grupo == data.Grupo);
            }

            return query;
        }

        public async Task<PagedResult<TLogins>> ConsultarT_loginsFiltro(PresentarT_loginsFiltro data)
        {
            var query = FilterJoin(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.Login,
                           item.Grupo,
                           item.NombreUsuario,
                           item.Rol,
                           item.Administrador,
                           item.EMail,
                           item.Activo,
                           item.IdReunion,
                       }
                into res
                       select new TLogins()
                       {
                           Login = res.Key.Login,
                           Grupo = res.Key.Grupo,
                           NombreUsuario = res.Key.NombreUsuario,
                           Rol = res.Key.Rol,
                           Administrador = res.Key.Administrador,
                           EMail = res.Key.EMail,
                           Activo = res.Key.Activo,
                           IdReunion = res.Key.IdReunion,
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<TLogins> FilterJoin(PresentarT_loginsFiltro data)
        {
            var query = _dbContext.JoinT_loginsPermisos().AsQueryable();

            if (data.id_reunion > 0)
            {
                query = query.Where(x => x.IdReunion == data.id_reunion);
            }

            return query;
        }
    }
}

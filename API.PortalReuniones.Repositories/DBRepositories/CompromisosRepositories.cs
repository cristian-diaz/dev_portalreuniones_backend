using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Paged;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Repositories.Middle;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using API.PortalReuniones.Entities.EntitiesExtension;

namespace API.PortalReuniones.Repositories.DBRepositories
{
    public class CompromisosRepositories : ICompromisosRepositories
    {
        private readonly PortalReunionesContext _dbContext;
        public CompromisosRepositories(PortalReunionesContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PagedResult<Compromisos>> ConsultarCompromiso(CompromisosFiltro data)
        {
            var query = Filter(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.NoId,
                           item.Tipo,
                           item.Tema,
                           item.Nombre,
                           item.Accion,
                           item.Solucion,
                           item.ArchivoAsociado,
                           item.Responsable,
                           item.FechaI,
                           item.FechaC,
                           item.Status,
                           item.Email,
                           item.FechaReunion,
                           item.HoraReunion,
                           item.NombreUsuario
                       }
                into res
                       select new Compromisos()
                       {
                           NoId = res.Key.NoId,
                           Tipo = res.Key.Tipo,
                           Tema = res.Key.Tema,
                           Nombre = res.Key.Nombre,
                           Accion = res.Key.Accion,
                           Solucion = res.Key.Solucion,
                           ArchivoAsociado = res.Key.ArchivoAsociado,
                           Responsable = res.Key.Responsable,
                           FechaI = res.Key.FechaI,
                           FechaC = res.Key.FechaC,
                           Status = res.Key.Status,
                           Email = res.Key.Email,
                           FechaReunion = res.Key.FechaReunion,
                           HoraReunion = res.Key.HoraReunion,
                           NombreUsuario = res.Key.NombreUsuario,
                       }).AsQueryable().OrderByDescending(c => c.FechaReunion);

            var result = await sql.Paginate(data.Pagina, data.Size);

            return result;
        }
        private IQueryable<Compromisos> Filter(CompromisosFiltro data)
        {
            var query = _dbContext.JoinCompromisos(0, !string.IsNullOrWhiteSpace(data.UsuarioRegistra)).AsQueryable();

            if (!string.IsNullOrWhiteSpace(data.UsuarioRegistra))
            {
                query = query.Where(x => x.UsuarioRegistra == data.UsuarioRegistra);
            }

            if (!string.IsNullOrWhiteSpace(data.Tipo))
            {
                query = query.Where(x => x.Tipo == data.Tipo);
            }

            if (!string.IsNullOrWhiteSpace(data.Tema))
            {
                query = query.Where(x => x.Tema.Contains(data.Tema));
            }

            if (!string.IsNullOrWhiteSpace(data.Nombre))
            {
                query = query.Where(x => x.Nombre.Contains(data.Nombre));
            }

            if (!(data.NoID==null || data.NoID==0))
            {
                query = query.Where(x => x.NoId == data.NoID);
            }

            if (!string.IsNullOrWhiteSpace(data.Status))
            {
                query = query.Where(x => x.Status == data.Status);
            }

            if (data.FechaR != null)
            {
                if(string.IsNullOrWhiteSpace(data.OperatorFR)){
                    query = query.Where(x => x.FechaReunion == data.FechaR);
                }else{
                    switch(data.OperatorFR){
                        case "=":
                            query = query.Where(x => x.FechaReunion == data.FechaR);
                            break;
                        case ">":
                            query = query.Where(x => x.FechaReunion > data.FechaR);
                            break;
                        case "<":
                            query = query.Where(x => x.FechaReunion < data.FechaR);
                            break;
                        case ">=":
                            query = query.Where(x => x.FechaReunion >= data.FechaR);
                            break;
                        case "<=":
                            query = query.Where(x => x.FechaReunion <= data.FechaR);
                            break;
                    }
                }
                
            }

            if (data.FechaI != null)
            {
                if(string.IsNullOrWhiteSpace(data.OperatorFI)){
                    query = query.Where(x => x.FechaI == data.FechaI);
                }else{
                    switch(data.OperatorFI){
                        case "=":
                            query = query.Where(x => x.FechaI == data.FechaI);
                            break;
                        case ">":
                            query = query.Where(x => x.FechaI > data.FechaI);
                            break;
                        case "<":
                            query = query.Where(x => x.FechaI < data.FechaI);
                            break;
                        case ">=":
                            query = query.Where(x => x.FechaI >= data.FechaI);
                            break;
                        case "<=":
                            query = query.Where(x => x.FechaI <= data.FechaI);
                            break;
                    }
                }
                
            }

            return query;
        }

        public async Task<PagedResult<Compromisos>> ConsultarCompromisoFiltros(PresentarCompromisosFiltro data)
        {
            var query = FilterFiltros(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.NoId,
                           item.Tipo,
                           item.Nombre,
                           item.Accion,
                           item.Solucion,
                           item.ArchivoAsociado,
                           item.Responsable,
                           item.FechaI,
                           item.FechaC,
                           item.Status,
                           item.Reunion,
                           item.Tema,
                           item.Delegado,
                           item.NoDias,
                           item.NombreUsuario,
                           item.Login
                       }
                into res
                       select new Compromisos()
                       {
                           NoId = res.Key.NoId,
                           Tipo = res.Key.Tipo,
                           Nombre = res.Key.Nombre,
                           Accion = res.Key.Accion,
                           Solucion = res.Key.Solucion,
                           ArchivoAsociado = res.Key.ArchivoAsociado,
                           Responsable = res.Key.Responsable,
                           FechaI = res.Key.FechaI,
                           FechaC = res.Key.FechaC,
                           Status = res.Key.Status,
                           Reunion = res.Key.Reunion,
                           Tema = res.Key.Tema,
                           Delegado = res.Key.Delegado,
                           NoDias = res.Key.NoDias,
                           NombreUsuario = res.Key.NombreUsuario,
                           Login = res.Key.Login
                       }).AsQueryable().OrderByDescending(c => c.FechaC).ThenByDescending(c => c.FechaI);

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<Compromisos> FilterFiltros(PresentarCompromisosFiltro data)
        {
            var query = _dbContext.JoinCompromisosFiltros(0, data.Administrador).AsQueryable();

            if (!string.IsNullOrWhiteSpace(data.Login))
            {
                query = query.Where(x => x.Login == data.Login);
            }

            if (!string.IsNullOrWhiteSpace(data.Reunion))
            {
                query = query.Where(x => x.Reunion == data.Reunion);
            }

            if (!string.IsNullOrWhiteSpace(data.Status))
            {
                query = query.Where(x => x.Status == data.Status);
            }

            if (data.NoID > 0)
            {
                query = query.Where(x => x.NoId == data.NoID);
            }

            if (!string.IsNullOrWhiteSpace(data.Tema))
            {
                query = query.Where(x => x.Tema == data.Tema);
            }

            if (!string.IsNullOrWhiteSpace(data.Responsable))
            {
                query = query.Where(x => x.Responsable == data.Responsable);
            }

            if (!string.IsNullOrWhiteSpace(data.Delegado))
            {
                query = query.Where(x => x.Delegado == data.Delegado);
            }

            switch (data.CondicionFI)
            {
                case "<":
                    query = query.Where(x => x.FechaI <= data.FechaI);
                    break;
                case ">":
                    query = query.Where(x => x.FechaI >= data.FechaI);
                    break;
                case "=":
                    query = query.Where(x => x.FechaI == data.FechaI);
                    break;
                default:
                    break;
            }

            switch (data.CondicionFC)
            {
                case "<":
                    query = query.Where(x => x.FechaC <= data.FechaC);
                    break;
                case ">":
                    query = query.Where(x => x.FechaC >= data.FechaC);
                    break;
                case "=":
                    query = query.Where(x => x.FechaC == data.FechaC);
                    break;
                default:
                    break;
            }

            if (data.FechaI != null)
            {
                query = query.Where(x => x.FechaI == data.FechaI);
            }

            if (data.FechaC != null)
            {
                query = query.Where(x => x.FechaI == data.FechaC);
            }

            if (data.NoDias > 0)
            {
                query = query.Where(x => x.NoDias == data.NoDias);
            }

            return query;
        }

        public async Task<PagedResult<Compromisos>> ConsultarCompromisoActaFiltros(CompromisosActaFiltro data)
        {
            var query = FilterActaFiltros(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.NoId,
                           item.Tipo,
                           item.Nombre,
                           item.Accion,
                           item.Solucion,
                           item.ArchivoAsociado,
                           item.Responsable,
                           item.FechaI,
                           item.FechaC,
                           item.Status,
                           item.Delegado,
                           item.State,
                           item.Tema,
                           item.IdActa
                       }
                into res
                       select new Compromisos()
                       {
                           NoId = res.Key.NoId,
                           Tipo = res.Key.Tipo,
                           Nombre = res.Key.Nombre,
                           Accion = res.Key.Accion,
                           Solucion = res.Key.Solucion,
                           ArchivoAsociado = res.Key.ArchivoAsociado,
                           Responsable = res.Key.Responsable,
                           FechaI = res.Key.FechaI,
                           FechaC = res.Key.FechaC,
                           Status = res.Key.Status,
                           Tema = res.Key.Tema,
                           Delegado = res.Key.Delegado,
                           State = res.Key.State,
                           IdActa = res.Key.IdActa
                       }).AsQueryable();

            var result = await sql.Paginate(data.Pagina, data.Size);

            return result;
        }

        private IQueryable<Compromisos> FilterActaFiltros(CompromisosActaFiltro data)
        {
            var query = _dbContext.JoinCompromisosActaFiltros(data.State).AsQueryable();

            if (data.IdActa != null)
            {
                query = query.Where(x => x.IdActa == data.IdActa);
            }

            return query;
        }

        public async Task<int> GuardarCompromiso(Compromisos data)
        {
            _dbContext.Compromisos.Add(data);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<int> ActualizarCompromisos(Compromisos viewModel)
        {
            var dato = _dbContext.Compromisos.Where(m => m.NoId == viewModel.NoId).FirstOrDefault();
            if (viewModel.Tipo != null)
                dato.Tipo = viewModel.Tipo;
            if (viewModel.Nombre != null)
                dato.Nombre = viewModel.Nombre;
            if (viewModel.Tema != null)
                dato.Tema = viewModel.Tema;
            if (viewModel.Accion != null)
                dato.Accion = viewModel.Accion;
            if (viewModel.Responsable != null)
                dato.Responsable = viewModel.Responsable;
            if (viewModel.FechaI != null)
                dato.FechaI = viewModel.FechaI;
            if (viewModel.FechaC != null)
                dato.FechaC = viewModel.FechaC;
            if (viewModel.Status != null)
                dato.Status = viewModel.Status;
            if (viewModel.Login != null)
                dato.Login = viewModel.Login;
            if (viewModel.Solucion != null)
                dato.Solucion = viewModel.Solucion;
            if (viewModel.ArchivoAsociado != null)
                dato.ArchivoAsociado = viewModel.ArchivoAsociado;
            if (dato.Verficado != viewModel.Verficado)
                dato.Verficado = viewModel.Verficado;
            if (viewModel.FechaRealCierre != null)
                dato.FechaRealCierre = viewModel.FechaRealCierre;
            if (viewModel.HoraCierre != null)
                dato.HoraCierre = viewModel.HoraCierre;
            if (viewModel.Priorizacion != null)
                dato.Priorizacion = viewModel.Priorizacion;
            if (viewModel.Categoria != null)
                dato.Categoria = viewModel.Categoria;
            if (viewModel.IdPeriodicidad != null)
                dato.IdPeriodicidad = viewModel.IdPeriodicidad;
            if (viewModel.FechaEnvioMail != null)
                dato.FechaEnvioMail = viewModel.FechaEnvioMail;
            if (viewModel.Delegado != null)
                dato.Delegado = viewModel.Delegado;
            if (viewModel.FechaReunion != null)
                dato.FechaReunion = viewModel.FechaReunion;
            if (viewModel.HoraReunion != null)
                dato.HoraReunion = viewModel.HoraReunion;
            if (viewModel.UsuarioRegistra != null)
                dato.UsuarioRegistra = viewModel.UsuarioRegistra;

            _dbContext.Entry(dato).State = EntityState.Modified;
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<PagedResult<ReporteCompromisos>> ConsultarReporteCompromisos(ReporteCompromisosFiltro data)
        {
            var query = FiltroReporteCompromisos(data);
            var sql = (from item in query
                       group item by new
                       {
                           item.Tipo,
                           item.Tema,
                           item.Accion,
                           item.Responsable,
                           item.Solucion,
                           item.FechaC,
                           item.Status,
                           item.Verificado,
                           item.NombreUsuario,
                       }
                into res
                       select new ReporteCompromisos()
                       {
                           Tipo = res.Key.Tipo,
                           Tema = res.Key.Tema,
                           Accion = res.Key.Accion,
                           Responsable = res.Key.Responsable,
                           Solucion = res.Key.Solucion,
                           FechaC = res.Key.FechaC,
                           Status = res.Key.Status,
                           Verificado = res.Key.Verificado,
                           NombreUsuario = res.Key.NombreUsuario,
                       }).AsQueryable();

            var result = await sql.Paginate(data.Page, data.Size);

            return result;
        }

        private IQueryable<ReporteCompromisos> FiltroReporteCompromisos(ReporteCompromisosFiltro data)
        {
            IQueryable<ReporteCompromisos> query;
            try
            {
                query = _dbContext.JoinCompromisosReporte().AsQueryable();

                if (!string.IsNullOrWhiteSpace(data.tema))
                {
                    query = query.Where(x => x.Tema == data.tema);
                }

                if (!string.IsNullOrWhiteSpace(data.accion))
                {
                    query = query.Where(x => x.Accion == data.accion);
                }

                if (!string.IsNullOrWhiteSpace(data.tipo))
                {
                    query = query.Where(x => x.Tipo == data.tipo);
                }

                if (!string.IsNullOrWhiteSpace(data.responsable))
                {
                    query = query.Where(x => x.Responsable == data.responsable);
                }

                //query = query.Where(x => x.state == data.state);
            }
            catch (System.Exception)
            {

                throw;
            }
            return query;
        }
    }
}
﻿using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace API.PortalReuniones.Repositories.DBRepositories
{
    public class Acta_CompromisosRepositories : IActa_CompromisosRepositories
    {
        private readonly PortalReunionesContext _dbContext;
        public Acta_CompromisosRepositories(PortalReunionesContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> GuardarActa_Compromisos(ActaCompromisos data)
        {
            _dbContext.Acta_Compromisos.Add(data);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<int> ActualizarActa_Compromisos(ActaCompromisos viewModel)
        {
            var dato = _dbContext.Acta_Compromisos.Where(m => m.IdActa == viewModel.IdActa && m.IdCompromisos == viewModel.IdCompromisos).FirstOrDefault();
            if (viewModel.IdActa > 0)
                dato.IdActa = viewModel.IdActa;
            if (viewModel.IdCompromisos > 0)
                dato.IdCompromisos = viewModel.IdCompromisos;

            _dbContext.Entry(dato).State = EntityState.Modified;
            return await _dbContext.SaveChangesAsync();
        }
    }
}

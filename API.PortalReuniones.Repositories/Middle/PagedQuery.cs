using API.PortalReuniones.Entities.Paged;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
namespace API.PortalReuniones.Repositories.Middle
{
    public static class PagedQuery
    {
        public static async Task<PagedResult<T>> Paginate<T>(this IQueryable<T> query, int page, int pageSize, bool asList = false) where T : class
        {
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = !asList ? query.Count(): query.ToList().Count
            };
            try
            {
                if (page == 0 && pageSize == 0)
                {
                    result.Results = !asList ? await query.ToListAsync() : query.ToList();
                    return result;
                }

                var pageCount = (double)result.RowCount / pageSize;
                result.PageCount = (int)Math.Ceiling(pageCount);

                var skip = (page - 1) * pageSize;
                var sql = query.Skip(skip).Take(pageSize).AsQueryable();
                result.Results = !asList ? await sql.ToListAsync(): sql.ToList();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }

            return result;
        }
    }
}

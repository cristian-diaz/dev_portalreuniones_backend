﻿using API.PortalReuniones.BussinesRules;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace API.PortalReuniones.Controllers
{
    [ApiController]
    [Route("api/v1/tlogin")]
    public class T_loginsController : ControllerBase
    {
        readonly T_loginsBussinesRules _bussines;
        public T_loginsController(IT_loginsRepositories repositories)
        {
            _bussines = new T_loginsBussinesRules(repositories);
        }

        [HttpGet]
        public async Task<ActionResult> GetConsultarT_loginsAsync([FromQuery] string Login, [FromQuery] bool? grupo, [FromQuery] string nombreusuario, [FromQuery] int? Rol, [FromQuery] int administrador, [FromQuery] string EMail, [FromQuery] bool? Activo, [FromQuery] int Page , [FromQuery] int Size )
        {
            var response = new ResponseBase<List<TLogins>>();
            try
            {
                var datatl = new TLoginsFiltro
                {
                    Login = Login,
                    Grupo = grupo,
                    NombreUsuario = nombreusuario,
                    Rol = Rol,
                    Administrador = administrador,
                    EMail = EMail,
                    Activo = Activo,
                    Page = Page,
                    Size = Size,
                };
                var tiporeunion = await _bussines.ConsultarT_logins(datatl);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = tiporeunion.Results;
                response.Count = tiporeunion.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        [HttpGet("gFiltro")]
        public async Task<ActionResult> GetConsultarT_loginsLogin([FromQuery] int id_reunion, [FromQuery] int Page = 1, [FromQuery] int Size = 20)
        {
            var response = new ResponseBase<List<TLogins>>();
            try
            {
                var datatl = new PresentarT_loginsFiltro 
                {
                    id_reunion = id_reunion,
                    Page = Page,
                    Size = Size,
                };
                var tiporeunion = await _bussines.ConsultarT_loginsFiltro(datatl);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = tiporeunion.Results;
                response.Count = tiporeunion.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }
    }
}

﻿using API.PortalReuniones.BussinesRules;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace API.PortalReuniones.Controllers
{
    [ApiController]
    [Route("api/v1/ccambios")]
    public class ControlCambiosController : ControllerBase
    {
        private readonly ControlCambiosBussinesRules _bussines;

        public ControlCambiosController(IControlCambiosRepositories repositories)
        {
            _bussines = new ControlCambiosBussinesRules(repositories);
        }

        //[HttpGet]
        //public async Task<ActionResult> GetConsultarControlCambios([FromQuery] int? NoID, [FromQuery] int? CompromisoID, [FromQuery] string TIPO, [FromQuery] string TEMA, [FromQuery] string Accion, [FromQuery] string RESPONSABLE, [FromQuery] string FECHAI, [FromQuery] string FECHAC, [FromQuery] string delegado, [FromQuery] string OperatorFI, [FromQuery] DateTime? fechai, [FromQuery] string OperatorFR, [FromQuery] DateTime? fechar, [FromQuery] int pagina = 1, [FromQuery] int size = 20)
        //{
        //    var response = new ResponseBase<List<ControlCambios>>();
        //    try
        //    {
        //        var datacc = new ControlCambiosFiltro
        //        {
        //            Page = pagina,
        //            Size = size,
        //            noID = noID,
        //            Tipo = Tipo,
        //            TEMA = tema,
        //            NOMBRE = nombre,
        //            STATUS = status,
        //            RESPONSABLE = responsable,
        //            Delegado = delegado,
        //            OperatorFI = OperatorFI,
        //            FECHAI = fechai,
        //            OperatorFR = OperatorFR,
        //            fecha_reunion = fechar,
        //            Usuario_registra = Usuario_registra,
        //        };
        //        var compromisos = await _bussines.ConsultarControlCambios(datacc);
        //        response.Code = (int)HttpStatusCode.OK;
        //        response.Data = compromisos.Results;
        //        response.Count = compromisos.RowCount;
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Code = (int)HttpStatusCode.InternalServerError;
        //        response.Message = ex.Message;
        //    }
        //    return StatusCode(response.Code, response);
        //}

        [HttpPut]
        public async Task<ActionResult> PutActualizarControlCambios(ControlCambios model)
        {
            var response = new ResponseBase<Compromisos>();
            try
            {
                var comp = await _bussines.ActualizarControlCambios(model);
                if (comp == 1)
                {
                    response.Code = (int)HttpStatusCode.OK;
                }
                else
                {
                    response.Code = (int)HttpStatusCode.BadRequest;
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }
    }
}

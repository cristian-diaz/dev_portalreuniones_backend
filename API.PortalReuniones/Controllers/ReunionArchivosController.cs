﻿using API.PortalReuniones.BussinesRules;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace API.PortalReuniones.Controllers
{
    [ApiController]
    [Route("api/v1/rarchivos")]
    public class ReunionArchivosController : ControllerBase
    {
        private readonly ReunionArchivosBussinesRules _bussines;
        public ReunionArchivosController(IReunionArchivosRepositories repositories)
        {
            _bussines = new ReunionArchivosBussinesRules(repositories);
        }

        [HttpGet]
        public async Task<ActionResult> GetConsultarReunionArchivos([FromQuery] int? Id, [FromQuery] int? Reunion, [FromQuery] string nombre, [FromQuery] string ruta, [FromQuery] string usuario, [FromQuery] string tp_Documento, [FromQuery] int pagina = 1, [FromQuery] int size = 20)
        {
            var response = new ResponseBase<List<ReunionArchivos>>();
            try
            {
                var datacomp = new ReunionArchivosFiltro
                {
                    Page = pagina,
                    Size = size,
                    Id = Id,
                    Reunion = Reunion,
                    Nombre = nombre,
                    Ruta = ruta,
                    Usuario = usuario,
                    TpDocumento = tp_Documento
                };
                var compromisos = await _bussines.ConsultarReunionArchivos(datacomp);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = compromisos.Results;
                response.Count = compromisos.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }
    }
}

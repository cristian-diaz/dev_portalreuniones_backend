﻿using API.PortalReuniones.BussinesRules;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Responses;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace API.PortalReuniones.Controllers
{
    [ApiController]
    [Route("api/v1/compromisos")]
    public class CompromisosController : ControllerBase
    {
        private readonly CompromisosBussinesRules _bussines;

        public CompromisosController(ICompromisosRepositories repositories)
        {
            _bussines = new CompromisosBussinesRules(repositories);
        }

        [HttpPost]
        public async Task<ActionResult> PostGuardarCompromiso(Compromisos model)
        {
            var response = new ResponseBase<Compromisos>();
            try
            {
                var comp = await _bussines.GuardarCompromiso(model);
                if (comp == 1)
                {
                    response.Code = (int)HttpStatusCode.OK;
                }
                else
                {
                    response.Code = (int)HttpStatusCode.BadRequest;
                    response.Message = "Ocurrio un error al registrar";
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        [HttpGet]
        public async Task<ActionResult> GetConsultarCompromiso([FromQuery] int? NoID, [FromQuery] string UsuarioRegistra, [FromQuery] string Tipo, [FromQuery] string Tema, [FromQuery] string Status, [FromQuery] string Nombre, [FromQuery] string Responsable, [FromQuery] string Delegado, [FromQuery] string OperatorFI, [FromQuery] DateTime? FechaI, [FromQuery] string OperatorFR, [FromQuery] DateTime? FechaR, [FromQuery] int Pagina = 1, [FromQuery] int Size = 20)
        {
            var response = new ResponseBase<List<Compromisos>>();
            try
            {
                var datacomp = new CompromisosFiltro
                {
                    Pagina = Pagina,
                    Size = Size,
                    NoID = NoID ,
                    Tipo = Tipo,
                    Tema = Tema,
                    Nombre = Nombre,
                    Status = Status,
                    Responsable = Responsable,
                    Delegado = Delegado,
                    OperatorFI = OperatorFI,
                    FechaI = FechaI,
                    OperatorFR = OperatorFR,
                    FechaR = FechaR,
                    UsuarioRegistra = UsuarioRegistra
                };
                var compromisos = await _bussines.ConsultarCompromiso(datacomp);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = compromisos.Results;
                response.Count = compromisos.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        [HttpDelete]
        public async Task<int> PostBorrarCompromisoID([FromBody] Compromisos model)
        {
            return await _bussines.BorrarCompromiso(model);
        }

        [HttpGet("gfiltros")]
        public async Task<ActionResult> GetConsultarCompromisoTipoReuinionAsync([FromQuery] bool Administrador, [FromQuery] string Login, [FromQuery] string Reunion, [FromQuery] string Status, [FromQuery] bool Verificado, [FromQuery] int NoID, [FromQuery] string Tema, [FromQuery] string Responsable, [FromQuery] string Delegado, [FromQuery] string CondicionFI, [FromQuery] DateTime? FechaI, [FromQuery] string CondicionFC, [FromQuery] DateTime? FechaC, [FromQuery] short NoDias, int pagina = 1, int size = 20)
        {
            var response = new ResponseBase<List<Compromisos>>();
            try
            {
                var datacomp = new PresentarCompromisosFiltro
                {
                    Page = pagina,
                    Size = size,
                    Administrador = Administrador,
                    Login = Login,
                    Reunion = Reunion,
                    Status = Status,
                    Verificado = Verificado,
                    NoID = NoID,
                    Tema = Tema,
                    Responsable = Responsable,
                    Delegado = Delegado,
                    CondicionFI = CondicionFI,
                    FechaI = FechaI,
                    CondicionFC = CondicionFC,
                    FechaC = FechaC,
                    NoDias = NoDias
                };
                var compromisos = await _bussines.ConsultarCompromisoFiltros(datacomp);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = compromisos.Results;
                response.Count = compromisos.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        [HttpPut]
        public async Task<ActionResult> PutActualizarCompromiso(Compromisos model)
        {
            var response = new ResponseBase<Compromisos>();
            try
            {
                var comp = await _bussines.ActualizarCompromiso(model);
                if (comp == 1)
                {
                    response.Code = (int)HttpStatusCode.OK;
                }
                else
                {
                    response.Code = (int)HttpStatusCode.BadRequest;
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        [HttpGet("gfiltrosActa")]
        public async Task<ActionResult> GetConsultarCompromisoFiltrosActa([FromQuery] int IdActa, int Pagina = 1, int Size = 20)
        {
            var response = new ResponseBase<List<Compromisos>>();
            try
            {
                var datacomp = new CompromisosActaFiltro
                {
                    IdActa = IdActa,
                    Pagina = Pagina,
                    Size = Size
                };
                var compromisos = await _bussines.ConsultarCompromisoActaFiltros(datacomp);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = compromisos.Results;
                response.Count = compromisos.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }
    }
}

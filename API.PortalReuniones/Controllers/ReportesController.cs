﻿using API.PortalReuniones.BussinesRules;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.EntitiesExtension;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Responses;
using API.PortalReuniones.Repositories;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace API.PortalReuniones.Controllers
{
    [ApiController]
    [Route("api/v1/Reportes")]
    public class ReportesController : ControllerBase
    {
        readonly CompromisosBussinesRules _compromisosRepositories;
        readonly TipoReunionBussinesRules _tipoReunionbussines;
        readonly ResultadosEvalBussinesRules _resultadosEvalBussinesRules;
        public ReportesController(ICompromisosRepositories compromisoRepositories, ITipoReunionRepositories tipoReunionRepositories, IResultadosEvalRepositories resultados_EvalReporsitories)
        {
            _compromisosRepositories = new CompromisosBussinesRules(compromisoRepositories);
            _tipoReunionbussines = new TipoReunionBussinesRules(tipoReunionRepositories);
            _resultadosEvalBussinesRules = new ResultadosEvalBussinesRules(resultados_EvalReporsitories);
        }

        //[HttpGet("ReporteTReunion")]
        //public async Task<ActionResult> GetConsultarTipoReunion([FromQuery] int Id, [FromQuery] string Nombre, [FromQuery] decimal? Activa, [FromQuery] string tipo_nm, [FromQuery] int? tip_asoc, [FromQuery] DateTime? horaInicio, [FromQuery] DateTime? horaFin, [FromQuery] int pagina, [FromQuery] int size)
        //{
        //    var response = new ResponseBase<List<TipoReunion>>();
        //    try
        //    {
        //        var datatr = new TipoReunionFiltro
        //        {
        //            Id = Id,
        //            Nombre = Nombre,
        //            Activa = Activa,
        //            tipo_nm = tipo_nm,
        //            tip_asoc = tip_asoc,
        //            horaInicio = horaInicio,
        //            horaFin = horaFin,
        //            Page = pagina,
        //            Size = size
        //        };
        //        var compromisos = await _tipoReunionbussines.ConsultarTipoReunion(datatr);
        //        response.Code = (int)HttpStatusCode.OK;
        //        response.Data = compromisos.Results;
        //        response.Count = compromisos.RowCount;
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Code = (int)HttpStatusCode.InternalServerError;
        //        response.Message = ex.Message;
        //    }
        //    return StatusCode(response.Code, response);
        //}

        [HttpPost("Descarga")]
        public async Task<IActionResult> DownloadExcelDocument(ReporteFiltro reporteFiltro)
        {
            try
            {
                string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                string fileName = null;

                if (reporteFiltro != null)
                {
                    List<TipoReunion> datosTipoReunion = new List<TipoReunion>();
                    List<ReporteCompromisos> datosCompromisos = new List<ReporteCompromisos>();
                    List<ReporteEvaluaciones> datosEvaluacion = new List<ReporteEvaluaciones>();
                    using (var workbook = new XLWorkbook())
                    {
                        IXLWorksheet worksheet = null;
                        switch (reporteFiltro.reporteCodigo)
                        {
                            case reporteCodigo.Listado_Reuniones:
                                fileName = "ReporteReuniones.xlsx";
                                var tr = await _tipoReunionbussines.ConsultarTipoReunion(reporteFiltro.reporteReunionesFiltro);
                                datosTipoReunion = tr.Results;
                                //datosTipoReunion = await _tipoReunionRepositories.ConsultarTipoReunion(reporteFiltro.reporteReunionesFiltro);
                                worksheet = workbook.Worksheets.Add("ListadoTipoReunion");
                                worksheet.Cell(1, 1).Value = "Reunión";
                                worksheet.Cell(1, 2).Value = "Tipo de Reunión";
                                worksheet.Cell(1, 3).Value = "Activa";
                                worksheet.Cell(1, 4).Value = "Hora de Inicio";
                                worksheet.Cell(1, 5).Value = "Hora de Fin";
                                worksheet.Cell(1, 6).Value = "Duración";
                                worksheet.Cell(1, 7).Value = "Automática";
                                for (int i = 1; i < datosTipoReunion.Count; i++)
                                {
                                    worksheet.Cell(i + 1, 1).Value = datosTipoReunion[i - 1].Nombre;
                                    worksheet.Cell(i + 1, 2).Value = datosTipoReunion[i - 1].TipAsoc;
                                    worksheet.Cell(i + 1, 3).Value = (datosTipoReunion[i - 1].Activa == -1) ? "SI" : "NO";
                                    worksheet.Cell(i + 1, 4).Value = datosTipoReunion[i - 1].HoraInicio;
                                    worksheet.Cell(i + 1, 5).Value = datosTipoReunion[i - 1].HoraFin;
                                    worksheet.Cell(i + 1, 6).Value = datosTipoReunion[i - 1].Duracion;
                                    worksheet.Cell(i + 1, 7).Value = (datosTipoReunion[i - 1].ValidarAsistentes == true) ? "SI" : "NO";
                                }
                                break;
                            case reporteCodigo.Listado_Compromisos:
                                fileName = "ReporteCompromisos.xlsx";
                                var comp = await _compromisosRepositories.ConsultarReporteCompromisos(reporteFiltro.reporteCompromisosFiltro);
                                datosCompromisos = comp.Results;
                                worksheet = workbook.Worksheets.Add("ListadoCompromisos");
                                worksheet.Cell(1, 1).Value = "Reunión";
                                worksheet.Cell(1, 2).Value = "Tema";
                                worksheet.Cell(1, 3).Value = "Acción";
                                worksheet.Cell(1, 4).Value = "Responsable";
                                worksheet.Cell(1, 5).Value = "Solución";
                                worksheet.Cell(1, 6).Value = "Fecha Compromiso";
                                worksheet.Cell(1, 7).Value = "Status";
                                worksheet.Cell(1, 8).Value = "Verificado";
                                for (int i = 1; i < datosCompromisos.Count; i++)
                                {
                                    worksheet.Cell(i + 1, 1).Value = datosCompromisos[i - 1].Tipo;
                                    worksheet.Cell(i + 1, 2).Value = datosCompromisos[i - 1].Tema;
                                    worksheet.Cell(i + 1, 3).Value = datosCompromisos[i - 1].Accion;
                                    worksheet.Cell(i + 1, 4).Value = datosCompromisos[i - 1].Responsable;
                                    worksheet.Cell(i + 1, 5).Value = datosCompromisos[i - 1].Solucion;
                                    worksheet.Cell(i + 1, 6).Value = datosCompromisos[i - 1].FechaC;
                                    worksheet.Cell(i + 1, 7).Value = datosCompromisos[i - 1].Status;
                                    worksheet.Cell(i + 1, 8).Value = (datosCompromisos[i - 1].Verificado == true) ? "SI" : "NO";
                                }
                                break;
                            case reporteCodigo.Listado_Evaluaciones:
                                fileName = "ReporteEvaluaciones.xlsx";
                                var eva = await _resultadosEvalBussinesRules.ConsultarReporteEvaluacion(reporteFiltro.reporteEvaluacionFiltro);
                                datosEvaluacion = eva.Results;
                                worksheet = workbook.Worksheets.Add("ListadoEvaluacion");
                                worksheet.Cell(1, 1).Value = "Reunión";
                                worksheet.Cell(1, 2).Value = "Item Evaluado";
                                worksheet.Cell(1, 3).Value = "Puntaje Item";
                                worksheet.Cell(1, 4).Value = "Puntaje Evaluación";
                                worksheet.Cell(1, 5).Value = "Evaluador";
                                worksheet.Cell(1, 6).Value = "Hora Inicio Planeada";
                                worksheet.Cell(1, 7).Value = "Hora Fin Planeada";
                                worksheet.Cell(1, 8).Value = "Hora Inicio Real";
                                worksheet.Cell(1, 9).Value = "Hora Fin Real";
                                worksheet.Cell(1, 10).Value = "Fecha Evaluación";
                                worksheet.Cell(1, 11).Value = "Lugar";
                                worksheet.Cell(1, 12).Value = "Comentario";
                                for (int i = 1; i < datosEvaluacion.Count; i++)
                                {
                                    worksheet.Cell(i + 1, 1).Value = datosEvaluacion[i - 1].Nombre;
                                    worksheet.Cell(i + 1, 2).Value = datosEvaluacion[i - 1].Item;
                                    worksheet.Cell(i + 1, 3).Value = datosEvaluacion[i - 1].Calificacion;
                                    worksheet.Cell(i + 1, 4).Value = datosEvaluacion[i - 1].TotalPuntos;
                                    worksheet.Cell(i + 1, 5).Value = datosEvaluacion[i - 1].NombreUsuario;
                                    worksheet.Cell(i + 1, 6).Value = datosEvaluacion[i - 1].HoraEstimadaInicio;
                                    worksheet.Cell(i + 1, 7).Value = datosEvaluacion[i - 1].HoraEstimadaFinal;
                                    worksheet.Cell(i + 1, 8).Value = datosEvaluacion[i - 1].HoraInicioReal;
                                    worksheet.Cell(i + 1, 9).Value = datosEvaluacion[i - 1].HoraFinalReal;
                                    worksheet.Cell(i + 1, 10).Value = datosEvaluacion[i - 1].Fecha;
                                    worksheet.Cell(i + 1, 11).Value = datosEvaluacion[i - 1].Lugar;
                                    worksheet.Cell(i + 1, 12).Value = datosEvaluacion[i - 1].Comentario;
                                }
                                break;
                            default:
                                fileName = null;
                                break;
                        }

                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            var content = stream.ToArray();
                            return File(content, contentType, fileName);
                        }
                    }

                }
                return BadRequest(new { code = true, mensaje = "Ocurrio un error generando el reporte" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { code = true, mensaje = ex.Message });
            }
        }
    }
}

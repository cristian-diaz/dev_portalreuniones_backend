﻿using API.PortalReuniones.BussinesRules;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace API.PortalReuniones.Controllers
{
    [ApiController]
    [Route("api/v1/itemEval")]
    public class ItemEvaluacionController : ControllerBase
    {
        private readonly ItemEvaluacionBussinesRules _bussines;

        public ItemEvaluacionController(IItemEvaluacionRepositories repositories)
        {
            _bussines = new ItemEvaluacionBussinesRules(repositories);
        }

        [HttpGet]
        public async Task<ActionResult> GetConsultarItemEvaluacion([FromQuery] int? IdItem, [FromQuery] string Item, [FromQuery] int? Peso, [FromQuery] string comentario, [FromQuery] string calificacion, [FromQuery] int? MaxPuntos, [FromQuery] bool? activo, [FromQuery] bool? calc, [FromQuery] int? ord, [FromQuery] int Page, [FromQuery] int Size)
        {
            var response = new ResponseBase<List<ItemEvaluacion>>();
            try
            {
                var dataie = new ItemEvaluacionFiltro
                {
                    IdItem = IdItem,
                    Item = Item,
                    Peso = Peso,
                    Comentario = comentario,
                    Calificacion = calificacion,
                    MaxPuntos = MaxPuntos,
                    Activo = activo,
                    Calc = calc,
                    Ord = ord,
                    Page = Page,
                    Size = Size,
                };
                var tr = await _bussines.ConsultarItemEvaluacion(dataie);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = tr.Results;
                response.Count = tr.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }
    }
}

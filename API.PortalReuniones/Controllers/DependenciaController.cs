﻿using API.PortalReuniones.BussinesRules;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.PortalReuniones.Controllers
{
    [ApiController]
    [Route("api/v1/dependencias")]
    public class DependenciaController : ControllerBase
    {
        private readonly DependenciaBussinesRules _bussines;
        public DependenciaController(IDependenciaRepositories repositories)
        {
            _bussines = new DependenciaBussinesRules(repositories);
        }

        [HttpGet]
        public async Task<ActionResult> GetDepencias()
        {
            var response = new ResponseBase<List<Dependencia>>();
            try
            {
                var dependencias = await _bussines.GetDepencias();
                response.Code = (int)HttpStatusCode.OK;
                response.Data = dependencias.Results;
                response.Count = dependencias.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        [HttpGet("gFiltro")]
        public async Task<ActionResult> GetConsultarDEPENDENCIA([FromQuery] string DEPENDENCIA_ID, [FromQuery] int TIPO_DEP_ID, [FromQuery] string DEP_PADRE_ID, [FromQuery] string DEPENDENCIA_NOMBRE, [FromQuery] int pagina, [FromQuery] int size)
        {
            var response = new ResponseBase<List<Dependencia>>();
            try
            {
                var dataacta = new DEPENDENCIAFiltro
                {
                    DependenciaId = DEPENDENCIA_ID,
                    TipoDepId = TIPO_DEP_ID,
                    DepPadreId = DEP_PADRE_ID,
                    DependenciaNombre = DEPENDENCIA_NOMBRE,
                    Page = pagina,
                    Size = size
                };
                var compromisos = await _bussines.ConsultarDEPENDENCIA(dataacta);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = compromisos.Results;
                response.Count = compromisos.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }
    }
}

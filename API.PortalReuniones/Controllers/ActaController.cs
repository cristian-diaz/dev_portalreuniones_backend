﻿using API.PortalReuniones.BussinesRules;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace API.PortalReuniones.Controllers
{
    [ApiController]
    [Route("api/v1/Acta")]
    public class ActaController : ControllerBase
    {
        readonly ActaBussinesRules _bussines;

        public ActaController(IActaRepositories repositories)
        {
            _bussines = new ActaBussinesRules(repositories);
        }

        [HttpPost]
        public async Task<ActionResult> PostGuardarActa([FromBody] Acta model)
        {
            var response = new ResponseBase<Acta>();
            try
            {
                var comp = await _bussines.GuardarActa(model);
                if (comp == 1)
                {
                    response.Code = (int)HttpStatusCode.OK;
                }
                else
                {
                    response.Code = (int)HttpStatusCode.BadRequest;
                    response.Message = "Ocurrio un error al registrar";
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        [HttpPut]
        public async Task<ActionResult> PutActualizarActa(Acta model)
        {
            var response = new ResponseBase<Acta>();
            try
            {
                var comp = await _bussines.ActualizarActa(model);
                if (comp == 1)
                {
                    response.Code = (int)HttpStatusCode.OK;
                }
                else
                {
                    response.Code = (int)HttpStatusCode.BadRequest;
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        [HttpGet]
        public async Task<ActionResult> GetConsultarActaAsync([FromQuery] int IdActa, [FromQuery] int NumActa, [FromQuery] DateTime? FechaActa, [FromQuery] int Estado, [FromQuery] string NombreActa, [FromQuery] string TemaActa, [FromQuery] DateTime? FechaEval, [FromQuery] DateTime? HoraInicio, [FromQuery] DateTime? HoraFin, [FromQuery] int Pagina, [FromQuery] int Size)
        {
            var response = new ResponseBase<List<Acta>>();
            try
            {
                var dataacta = new ActaFiltro
                {
                    IdActa = IdActa,
                    NumActa = NumActa,
                    FechaActa = FechaActa,
                    Estado = Estado,
                    NombreActa = NombreActa,
                    TemaActa = TemaActa,
                    FechaEval = FechaEval,
                    HoraInicio = HoraInicio,
                    HoraFin = HoraFin,
                    Page = Pagina,
                    Size = Size
                };
                var compromisos = await _bussines.ConsultarActa(dataacta);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = compromisos.Results;
                response.Count = compromisos.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }
    }
}

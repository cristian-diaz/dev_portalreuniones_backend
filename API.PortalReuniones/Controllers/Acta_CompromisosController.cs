﻿using API.PortalReuniones.BussinesRules;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace API.PortalReuniones.Controllers
{
    [ApiController]
    [Route("api/v1/ActaComp")]
    public class Acta_CompromisosController : ControllerBase
    {
        readonly Acta_CompromisosBussinesRules _bussines;

        public Acta_CompromisosController(IActa_CompromisosRepositories repositories)
        {
            _bussines = new Acta_CompromisosBussinesRules(repositories);
        }

        [HttpPost]
        public async Task<ActionResult> PostGuardarActa_Compromisos([FromBody] ActaCompromisos model)
        {
            var response = new ResponseBase<ActaCompromisos>();
            try
            {
                var comp = await _bussines.GuardarActa_Compromisos(model);
                if (comp == 1)
                {
                    response.Code = (int)HttpStatusCode.OK;
                }
                else
                {
                    response.Code = (int)HttpStatusCode.BadRequest;
                    response.Message = "Ocurrio un error al registrar";
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        [HttpPut]
        public async Task<ActionResult> PostActualizarActa_Compromisos(ActaCompromisos model)
        {
            var response = new ResponseBase<Acta>();
            try
            {
                var comp = await _bussines.ActualizarActa_Compromisos(model);
                if (comp == 1)
                {
                    response.Code = (int)HttpStatusCode.OK;
                }
                else
                {
                    response.Code = (int)HttpStatusCode.BadRequest;
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }
    }
}

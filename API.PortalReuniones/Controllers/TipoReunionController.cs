﻿using API.PortalReuniones.BussinesRules;
using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Entities.Filtro;
using API.PortalReuniones.Entities.Interfaces;
using API.PortalReuniones.Entities.Responses;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace API.PortalReuniones.Controllers
{
    [ApiController]
    [Route("api/v1/treunion")]
    public class TipoReunionController : ControllerBase
    {
        private readonly TipoReunionBussinesRules _bussines;

        public TipoReunionController(ITipoReunionRepositories repositories)
        {
            _bussines = new TipoReunionBussinesRules(repositories);
        }

        [HttpGet]
        public async Task<ActionResult> GetConsultarTipoReunion([FromQuery] int Id, [FromQuery] string Nombre, [FromQuery] decimal? Activa, [FromQuery] string TipoNm, [FromQuery] int? TipAsoc, [FromQuery] DateTime? HoraInicio, [FromQuery] DateTime? HoraFin, [FromQuery] int Page = 1, [FromQuery] int Size = 20)
        {
            var response = new ResponseBase<List<TipoReunion>>();
            try
            {
                var datatr = new TipoReunionFiltro
                {
                    Id = Id,
                    Nombre = Nombre,
                    Activa = Activa,
                    TipoNm = TipoNm,
                    TipAsoc = TipAsoc,
                    HoraInicio = HoraInicio,
                    HoraFin = HoraFin,
                    Page = Page,
                    Size = Size,
                };
                var tiporeunion = await _bussines.ConsultarTipoReunion(datatr);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = tiporeunion.Results;
                response.Count = tiporeunion.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        //[HttpGet("ConsultarTipoReunionNombre")]
        //public async Task<TipoReunion> GetConsultarTipoReunionNombreAsync(string nombre)
        //{
        //    return await _tipoReunionRepositories.ConsultarTipoReunionAsync(nombre);
        //}

        //[HttpPost("ConsultarTipoReunion")]
        //public async Task<ActionResult> GetConsultarTipoReunion(TipoReunionFiltro viewModel, int pagina = 1, int tampagina = 20)
        //{
        //    List<TipoReunion> tipoReuniones = new List<TipoReunion>();
        //    int paginaTotal = 0;
        //    try
        //    {
        //        var reponse = await _tipoReunionRepositories.ConsultarTipoReunion(viewModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(new { error = true, mensaje = ex.Message });
        //    }
        //    return Ok(new { code = false, mensaje = "Ok", data = tipoReuniones.Skip(tampagina * (pagina - 1)).Take(tampagina).ToList(), totalPaginas = paginaTotal });
        //}

        [HttpPost]
        public async Task<ActionResult> PostGuardarTipoReunion(TipoReunion data)
        {
            var response = new ResponseBase<TipoReunion>();
            try
            {
                var comp = await _bussines.GuardarTipoReunion(data);
                if (comp == 1)
                {
                    response.Code = (int)HttpStatusCode.OK;
                }
                else
                {
                    response.Code = (int)HttpStatusCode.BadRequest;
                    response.Message = "Ocurrio un error al registrar";
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        [HttpGet("gFiltro")]
        public async Task<ActionResult> GetConsultarTipoReunionLogin([FromQuery] string login, [FromQuery] string tipo_nm, [FromQuery] string permiso, [FromQuery] int Page = 1, [FromQuery] int Size = 20)
        {
            var response = new ResponseBase<List<TipoReunion>>();
            try
            {
                var datatr = new PresentarTipoReunionFiltro
                {
                    Login = login,
                    TipoNm = tipo_nm,
                    Permiso = permiso,
                    Page = Page,
                    Size = Size,
                };
                var tr = await _bussines.ConsultarTipoReunionFiltro(datatr);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = tr.Results;
                response.Count = tr.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }


        [HttpPut]
        public async Task<ActionResult> PutActualizarTipoReunion(TipoReunion data)
        {
            var response = new ResponseBase<TipoReunion>();
            try
            {
                var comp = await _bussines.ActualizarTipoReunion(data);
                if (comp == 1)
                {
                    response.Code = (int)HttpStatusCode.OK;
                }
                else
                {
                    response.Code = (int)HttpStatusCode.BadRequest;
                }
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }

        [HttpGet("gIndicador")]
        public async Task<ActionResult> GetConsultarTipoReunionIndicador([FromQuery] string tipo, [FromQuery] int Page, [FromQuery] int Size)
        {
            var response = new ResponseBase<List<TipoReunion>>();
            try
            {
                var datatr = new TipoReunionIndicadorFiltro
                {
                    tipo = tipo,
                    Page = Page,
                    Size = Size,
                };
                var tr = await _bussines.ConsultarTipoReunionIndicador(datatr);
                response.Code = (int)HttpStatusCode.OK;
                response.Data = tr.Results;
                response.Count = tr.RowCount;
            }
            catch (Exception ex)
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.Message;
            }
            return StatusCode(response.Code, response);
        }
    }
}

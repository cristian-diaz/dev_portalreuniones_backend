using API.PortalReuniones.Entities.Entities;
using API.PortalReuniones.Repositories.DBRepositories;
using API.PortalReuniones.Entities.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.ResponseCompression;
using System.IO.Compression;
using Microsoft.OpenApi.Models;

namespace API.PortalReuniones
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => 
                o.AddPolicy("PoliticaCORS", builder =>{builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();})
            );
            services.AddControllers();
            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true;
            })
                .AddXmlSerializerFormatters()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddDbContext<PortalReunionesContext>(options =>
                 options.UseSqlServer(
                     Configuration.GetConnectionString("PortalReunionesConnection")).EnableSensitiveDataLogging());
            services.AddSwaggerGen();
            services.AddTransient<IActa_CompromisosRepositories, Acta_CompromisosRepositories>();
            //services.AddTransient<IActa_ParticipanteRepositories, Acta_ParticipanteRepositories>();
            services.AddTransient<IActaRepositories, ActaRepositories>();
            services.AddTransient<IControlCambiosRepositories, ControlCambiosRepositories>();
            services.AddTransient<ICompromisosRepositories, CompromisosRepositories>();
            services.AddTransient<IT_loginsRepositories, T_loginsRepositories>();
            services.AddTransient<IResultadosEvalRepositories, ResultadosEvalRepositories>();
            services.AddTransient<ITipoReunionRepositories, TipoReunionRepositories>();
            services.AddTransient<IT_loginsRepositories, T_loginsRepositories>();
            services.AddTransient<IDependenciaRepositories, DependenciasRespositories>();
            services.AddTransient<IItemEvaluacionRepositories, ItemEvaluacionRepositories>();
            services.AddTransient<IReunionArchivosRepositories, ReunionArchivosRepositories>();
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Fastest);
            services.AddResponseCompression(options =>{
                options.Providers.Add<GzipCompressionProvider>();
            });
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo 
                {
                    Title = "API Portal compromisos",
                    Version = "v1",
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors("PoliticaCORS");
            //app.UseAuthorization();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Portal Reuniones");
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
